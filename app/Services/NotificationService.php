<?php

namespace App\Services;
use DB;
use DateTime;
use Illuminate\Http\Request;
use LaravelFCM\Message\Topics;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class NotificationService {
    
    public function IndividualNotification($device_token, $title, $message, $value){
        try {
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'value' => $value,
                'title' => $title,
                'body' => $message
            ]);
            $data = $dataBuilder->build();
            $messageResponse = FCM::sendTo($device_token, null, null, $data);

            return "Success";
        } catch (Exception $ex) {
            return "Failed";
        }
    }

    public function GroupNotification($topic_name, $title, $message, $value){
        try {
            $topic = new Topics();
            $topic->topic($topic_name);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData([
                'value' => $value,
                'title' => $title,
                'body' => $message
            ]);
            $data = $dataBuilder->build();
            $messageResponse = FCM::sendToTopic($topic, null, null, $data);

            return "Success";
        } catch (Exception $ex) {
            return "Failed";
        }
    }
}