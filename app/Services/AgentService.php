<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Loans;
use App\Models\GroupLoans;
use App\Services\LoanService;
use App\Models\AgentTransactions;
use App\Models\Commissions;
use App\Models\AgentNotifications;



// use Carbon\Carbon;

class AgentService {
    public function getProfileAgent($id)
    {
        $query = DB::table('m_agents')
                ->join('d_agents','d_agents.id_agent','=','m_agents.id')
                ->join('commissions','commissions.id_agent','=','m_agents.id')
                ->join('sub_districts', 'sub_districts.id', '=', 'd_agents.id_sub_district')
                ->join('regencies', 'regencies.id', '=', 'sub_districts.id_regency')
                ->join('provinces', 'provinces.id', '=', 'regencies.id_province')
                ->select('m_agents.id','m_agents.agent_local_id','m_agents.birth_date'
                    ,'m_agents.gender','d_agents.profil_image','d_agents.name','d_agents.email','d_agents.domicile_address'
                    ,'d_agents.phone_number','d_agents.last_education','d_agents.principal_taxpayer_id'
                    ,'d_agents.agent_family_card_id','d_agents.agent_code','d_agents.finance_balance'
                    ,'d_agents.rating', 'd_agents.rating_desc', 'd_agents.id_card_address', 'd_agents.id_sub_district'
                    ,'commissions.target','commissions.total_lended','commissions.commision_balance'
                    ,'sub_districts.sub_district_name', 'regencies.regency_name', 'provinces.province_name')
                ->where('m_agents.id', $id)
                ->first();
        $borrowerApplicant= Loans::where('id_agent', $id)
                ->whereNull('is_confirm_agent')
                ->where('loan_status','=','Belum Disetujui')
                ->count();

        $loanReguler= Loans::where('id_agent', $id)
                ->whereNull('group_name')
                ->where(function ($query) {
                    $query->orwhere('loan_status', '=', 'Belum Disetujui')
                          ->orwhere('loan_status', '=', 'Cicilan Sedang Berjalan')
                          ->orwhere('loan_status', '=', 'Pinjaman Disetujui');
                })
                ->count();
        $loanYetApproved= Loans::where('id_agent', $id)
                ->where('loan_status','=','Belum Disetujui')
                ->whereNull('group_name')
                ->count();
        $loanApproved= Loans::where('id_agent', $id)
                ->where('loan_status','=','Pinjaman Disetujui')
                ->whereNull('group_name')
                ->count();
        $loanOnProgress= Loans::where('id_agent', $id)
                ->where('loan_status','=','Cicilan Sedang Berjalan')
                ->whereNull('group_name')
                ->count();
        $loanService= new LoanService();
                $lateLoan = $loanService->sumLateLoan($id);
        $loanGroup= Loans::where('id_agent', $id)
                ->whereNotNull('group_name')
                ->count();
        $commission = Commissions::where('id_agent', $id)
                ->first();
        $query->total_regular_loan= $loanReguler;        
        $query->total_borrower_applicant= $borrowerApplicant; 
        $query->total_yet_approved= $loanYetApproved;
        $query->total_loan_approved= $loanApproved;
        $query->total_loan_on_progress= $loanOnProgress;
        $query->total_loan_late= $lateLoan;
        $query->total_loan_group= $loanGroup; 
        return $query;

    }
    public function getProfileHeadAgent($id)
    {
        $query = DB::table('m_head_agents')
                ->join('d_agents','d_agents.id_head_agent','=','m_head_agents.id')
                ->join('commissions','commissions.id_head_agent','=','m_head_agents.id')
                ->join('sub_districts', 'sub_districts.id', '=', 'd_agents.id_sub_district')
                ->join('regencies', 'regencies.id', '=', 'sub_districts.id_regency')
                ->join('provinces', 'provinces.id', '=', 'regencies.id_province')
                ->select('m_head_agents.id','m_head_agents.agent_local_id','m_head_agents.birth_date'
                    ,'m_head_agents.gender','d_agents.profil_image','d_agents.name','d_agents.email','d_agents.domicile_address'
                    ,'d_agents.phone_number','d_agents.last_education','d_agents.principal_taxpayer_id'
                    ,'d_agents.agent_family_card_id','d_agents.agent_code','d_agents.finance_balance'
                    ,'d_agents.rating', 'd_agents.rating_desc', 'd_agents.id_card_address', 'd_agents.id_sub_district'
                    ,'commissions.total_lended','commissions.commision_balance','commissions.target'
                    ,'sub_districts.sub_district_name', 'regencies.regency_name', 'provinces.province_name')
                ->where('m_head_agents.id', $id)
                ->first();
        $borrowerApplicant= Loans::where('id_head_agent', $id)
                ->whereNull('is_confirm_agent')
                ->where('loan_status','=','Belum Disetujui')
                ->count();
        $loanReguler= Loans::where('id_head_agent', $id)
                ->whereNull('group_name')
                ->where(function ($query) {
                    $query->orwhere('loan_status', '=', 'Belum Disetujui')
                          ->orwhere('loan_status', '=', 'Cicilan Sedang Berjalan')
                          ->orwhere('loan_status', '=', 'Pinjaman Disetujui');
                })
                ->count();
        $loanYetApproved= Loans::where('id_head_agent', $id)
                ->where('loan_status','=','Belum Disetujui')
                ->whereNull('group_name')
                ->count();
        $loanApproved= Loans::where('id_head_agent', $id)
                ->where('loan_status','=','Pinjaman Disetujui')
                ->whereNull('group_name')
                ->count();
        $loanOnProgress= Loans::where('id_head_agent', $id)
                ->where('loan_status','=','Cicilan Sedang Berjalan')
                ->whereNull('group_name')
                ->count();
        $loanService= new LoanService();
                $lateLoan = $loanService->sumLateLoan($id);
        $loanGroup= Loans::where('id_head_agent', $id)
                    ->whereNotNull('group_name')
                ->count();
        $commission = Commissions::where('id_head_agent', $id)
                ->first();
        $query->total_borrower_applicant= $borrowerApplicant; 
        $query->total_regular_loan= $loanReguler;        
        $query->total_yet_approved= $loanYetApproved;
        $query->total_loan_approved= $loanApproved;
        $query->total_loan_on_progress= $loanOnProgress;
        $query->total_loan_late= $lateLoan;
        $query->total_loan_group= $loanGroup; 
        return $query;
    }

    public function viewBorrowerApplicant($id){
        $borrowers= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image')
            ->where('loans.id_agent', $id)
            ->whereNull('loans.is_confirm_agent')
            ->where('loans.loan_status','=', 'Belum Disetujui')
            ->get();
        return $borrowers;
    }

    public function viewBorrowerApplicantHead($id){
        $borrowers= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image')
            ->where('loans.id_head_agent', $id)
            ->whereNull('loans.is_confirm_agent')
            ->where('loans.loan_status', '=','Belum Disetujui')
            ->get();
        return $borrowers;
    }

    public function searchBorrowerApplicant($idAgency, $localId){
        $borrowers= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image')
            ->where('loans.id_agent', $idAgency)
            ->whereNull('loans.is_confirm_agent')
            ->where('loans.loan_status' ,'=', 'Belum Disetujui')
            ->where('m_borrowers.borrower_local_id', $localId)
            ->first();
        return $borrowers;
    }

    public function searchBorrowerApplicantHead($idAgency, $localId){
        $borrowers= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower','loans.loan_category'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image')
            ->where('loans.id_head_agent', $idAgency)
            ->whereNull('loans.is_confirm_agent')
            ->where('loans.loan_status','=', 'Belum Disetujui')
            ->where('m_borrowers.borrower_local_id', $localId)
            ->first();
        return $borrowers;
    }

    public function searchNotYetLoan($idAgency, $localId){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_agent', $idAgency)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Belum Disetujui')
        ->where('m_borrowers.borrower_local_id', $localId)
        ->first();

        return $regulerLoan;
    }

    public function searchNotYetLoanHead($idAgency, $localId){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_head_agent', $idAgency)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Belum Disetujui')
        ->where('m_borrowers.borrower_local_id', $localId)
        ->first();

        return $regulerLoan;
    }

    public function searchApprovedLoan($idAgency, $localId){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_agent', $idAgency)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Pinjaman Disetujui')
        ->where('m_borrowers.borrower_local_id', $localId)
        ->first();

        return $regulerLoan;
    }

    public function searchApprovedLoanHead($idAgency, $localId){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_head_agent', $iidAgencyd)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Pinjaman Disetujui')
        ->where('m_borrowers.borrower_local_id', $localId)
        ->first();

        return $regulerLoan;
    }

    public function searchOnProgressLoan($idAgency, $localId){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_agent', $idAgency)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Cicilan Sedang Berjalan')
        ->where('m_borrowers.borrower_local_id', $localId)
        ->first();

            // jumlah cicilan yang sudah dibayar
            $installment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->where('loans.id_agent', $idAgency)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNotNull('installments.payment_date')
            ->where('loans.id', $regulerLoan->id_loan)
            ->count();

            $due_date= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('installments.due_date')
            ->where('loans.id_agent', $idAgency)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNull('installments.payment_date')
            ->where('loans.id', $regulerLoan->id_loan)
            ->first();

            $regulerLoan->count_installment=$installment;
            $regulerLoan->due_date=$due_date->due_date;
            $regulerLoan->total_installment= $regulerLoan->tenor;

        return $regulerLoan;
    }

    public function searchOnProgressLoanHead($idAgency, $localId){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_head_agent', $idAgency)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Cicilan Sedang Berjalan')
        ->where('m_borrowers.borrower_local_id', $localId)
        ->first();

            // jumlah cicilan yang sudah dibayar
            $installment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->where('loans.id_head_agent', $idAgency)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNotNull('installments.payment_date')
            ->where('loans.id', $regulerLoan->id_loan)
            ->count();

            $due_date= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('installments.due_date')
            ->where('loans.id_head_agent', $idAgency)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNull('installments.payment_date')
            ->where('loans.id', $regulerLoan->id_loan)
            ->first();

            $regulerLoan->count_installment=$installment;
            $regulerLoan->due_date=$due_date->due_date;
            $regulerLoan->total_installment= $regulerLoan->tenor;

        return $regulerLoan;
    }


    public function notYetLoan($id){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        // ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        // ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        // ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        // ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        // ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        // ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_agent', $id)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Belum Disetujui')
        ->get();

        return $regulerLoan;
    }

    public function approvedLoan($id){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        // ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        // ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        // ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        // ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        // ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        // ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_agent', $id)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Pinjaman Disetujui')
        ->get();

        return $regulerLoan;
    }

    public function onProgressLoan($id){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        // ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        // ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        // ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        // ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        // ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        // ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_agent', $id)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Cicilan Sedang Berjalan')
        ->get();

        foreach($regulerLoan as $item){
            // jumlah cicilan yang sudah dibayar
            $installment= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->where('loans.id_agent', $id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNotNull('installments.payment_date')
            ->where('loans.id', $item->id_loan)
            ->count();

            $due_date= DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->select('installments.due_date')
            ->where('loans.id_agent', $id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->whereNull('installments.payment_date')
            ->where('loans.id', $item->id_loan)
            ->first();

            // total cicilan
            $totalInstallment=DB::table('loans')
            ->join('installments','installments.id_loan','loans.id')
            ->where('loans.id_agent', $id)
            ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
            ->whereNull('loans.group_name')
            ->where('loans.id', $item->id_loan)
            ->count();

            $item->count_installment=$installment;
            $item->due_date=$due_date->due_date;
            $item->total_installment= $totalInstallment;
            $list[]= $item;
        }


        return $regulerLoan;
    }

    
    public function notYetLoanHead($id){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        // ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        // ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        // ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        // ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        // ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        // ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_head_agent', $id)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Belum Disetujui')
        ->get();

        return $regulerLoan;
    }

    public function approvedLoanHead($id){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        // ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        // ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        // ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        // ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        // ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        // ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_head_agent', $id)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Pinjaman Disetujui')
        ->get();

        return $regulerLoan;
    }

    public function onProgressLoanHead($id){
        $regulerLoan= DB::table('loans')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->join('date_loan','date_loan.id_loan', 'loans.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
        ,'loans.installment_nominal','loans.loan_status','loans.loan_category'
        ,'m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        // ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        // ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        // ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        // ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        // ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        // ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.withdraw_fund_date'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        )
        ->where('loans.id_head_agent', $id)
        ->whereNull('loans.group_name')
        ->where('loan_status', '=', 'Cicilan Sedang Berjalan')
        ->get();

        return $regulerLoan;
    }


    public function detailBorrowerApplicant($id, $idLoan){
        $borrowers= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image')
            ->where('loans.id_agent', $id)
            ->where('loans.id', $idLoan)
            ->first();
        return $borrowers;
    }

    public function detailBorrowerApplicantHead($id, $idLoan){
        $borrowers= DB::table('loans')
            ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
            ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
            ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
            ,'loans.loan_type','loans.loan_principal','loans.tenor','loans.installment_type'
            ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
            ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
            ,'d_borrowers.phone_number','d_borrowers.domicile_address'
            ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
            ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
            ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
            ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
            ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
            ,'d_borrowers.home_image','d_borrowers.salary_slip_image')
            ->where('loans.id_head_agent', $id)
            ->where('loans.id', $idLoan)
            ->first();
        return $borrowers;
    }

    public function detailRegulerLoan($id, $idLoan){
        $regulerLoan= DB::table('loans')
        ->join('date_loan','date_loan.id_loan','loans.id')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor'
        ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'loans.installment_type','loans.loan_category'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw','date_loan.submition_date'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.rejection_date','date_loan.withdraw_fund_date'
        ,'date_loan.cancellation_date','loans.loan_negotiation'
        )
        ->where('loans.id_agent', $id)
        ->where('loans.id',$idLoan)
        ->first();

        return $regulerLoan;
    }

    public function detailRegulerLoanHead($id, $idLoan){
        $regulerLoan= DB::table('loans')
        ->join('date_loan','date_loan.id_loan','loans.id')
        ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
        ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
        ->select('loans.id as id_loan','m_borrowers.id as id_borrower'
        ,'loans.loan_type','loans.loan_principal','loans.tenor'
        ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
        ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
        ,'d_borrowers.phone_number','d_borrowers.domicile_address'
        ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
        ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
        ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
        ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
        ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
        ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
        ,'loans.installment_type','loans.loan_category'
        ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
        ,'loans.is_withdraw'
        ,'date_loan.submition_date','date_loan.validation_date','date_loan.verification_date'
        ,'date_loan.approvement_date','date_loan.rejection_date','date_loan.withdraw_fund_date'
        ,'date_loan.cancellation_date','loans.loan_negotiation'        
        )
        ->where('loans.id_head_agent', $id)
        ->where('loans.id',$idLoan)
        ->first();

        return $regulerLoan;
    }

    public function historyIndividu($idAgency, $loanType){
        $date = new Carbon;
        $history= DB::table('loans')
                ->join('date_loan','date_loan.id_loan','loans.id')
                ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
                ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
                ->select('loans.id as id_loan','loans.loan_type','loans.loan_principal'
                ,'loans.tenor','loans.installment_nominal','loans.installment_type'
                ,'loans.loan_status','loans.loan_category'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number'
                , 'date_loan.submition_date','date_loan.validation_date'
                ,'date_loan.verification_date','date_loan.approvement_date','date_loan.withdraw_fund_date'
                ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
                ,'loans.is_withdraw')
                ->where('loans.id_agent', $idAgency)
                ->where('loans.loan_type', '=',$loanType)
                ->get();
            foreach($history as $item){
                if($item->loan_status == "Cicilan Sedang Berjalan"){
                // jumlah cicilan yang sudah dibayar
                    $installment= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.id_agent', $idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNotNull('installments.payment_date')
                    ->where('loans.id', $item->id_loan)
                    ->count();

                    $due_date= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->select('installments.due_date')
                    ->where('loans.id_agent', $idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNull('installments.payment_date')
                    ->where('loans.id', $item->id_loan)
                    ->first();

                    // total cicilan
                    $totalInstallment=DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.id_agent', $idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->where('loans.id', $item->id_loan)
                    ->count();

                    $lateLoan= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.id_agent', $idAgency)
                    ->where('loans.id', $item->id_loan)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNull('installments.payment_date')
                    // ->where('installments.due_date', '<', $date)
                    ->first();

                    $payment= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
                    ->where('loans.id_agent',$idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNull('installments.payment_date')
                    ->where('installments.due_date', '<', $date)
                    ->where('loans.id',$item->id_loan)
                    ->count();

                    $item->count_installment=$installment;
                    $item->due_date=$due_date->due_date;
                    $item->total_installment= $totalInstallment;
                    if($lateLoan->due_date < $date){
                        $item->late_loan='Cicilan Terlambat';
                        $item->total_late_installment= $payment;
                    } else{
                        $item->total_late_installment= 0;

                    }
                    $list[]= $item;
                }
            }
        return $history;     
    }    
    
    public function historyIndividuHead($idAgency, $loanType){
        $date = new Carbon;
        $history= DB::table('loans')
                ->join('date_loan','date_loan.id_loan','loans.id')
                ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
                ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
                ->select('loans.id as id_loan','loans.loan_type','loans.loan_principal'
                ,'loans.tenor','loans.installment_nominal','loans.installment_type'
                ,'loans.loan_status','loans.loan_category'
                ,'m_borrowers.borrower_local_id','d_borrowers.name','d_borrowers.phone_number'
                , 'date_loan.submition_date','date_loan.validation_date'
                ,'date_loan.verification_date','date_loan.approvement_date','date_loan.withdraw_fund_date'
                ,'loans.is_confirm_agent','loans.is_confirm_first_admin','loans.is_confirm_last_admin'
                ,'loans.is_withdraw')
                ->where('loans.id_head_agent', $idAgency)
                ->where('loans.loan_type', '=',$loanType)
                ->get();
            foreach($history as $item){
                if($item->loan_status == "Cicilan Sedang Berjalan"){
                // jumlah cicilan yang sudah dibayar
                    $installment= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.id_head_agent', $idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNotNull('installments.payment_date')
                    ->where('loans.id', $item->id_loan)
                    ->count();

                    $due_date= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->select('installments.due_date')
                    ->where('loans.id_head_agent', $idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNull('installments.payment_date')
                    ->where('loans.id', $item->id_loan)
                    ->first();

                    // total cicilan
                    $totalInstallment=DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.id_head_agent', $idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->where('loans.id', $item->id_loan)
                    ->count();

                    $lateLoan= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.id_head_agent', $idAgency)
                    ->where('loans.id', $item->id_loan)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNull('installments.payment_date')
                    // ->where('installments.due_date', '<', $date)
                    ->first();

                    $payment= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
                    ->where('loans.id_head_agent',$idAgency)
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNull('installments.payment_date')
                    ->where('installments.due_date', '<', $date)
                    ->where('loans.id',$item->id_loan)
                    ->count();

                    $item->count_installment=$installment;
                    $item->due_date=$due_date->due_date;
                    $item->total_installment= $totalInstallment;
                    if($lateLoan->due_date < $date){
                        $item->late_loan='Cicilan Terlambat';
                        $item->total_late_installment= $payment;
                    }
                    $list[]= $item;
                }
            }
        
                return $history;     
    } 

    public function viewNotification($idAgency){
        $allNotif= DB::table('agent_notifications')
                ->where('agent_notifications.id_agent',$idAgency)
                ->get();       

        return $allNotif;
    }

    public function viewNotificationHead($idAgency){
        $notif= DB::table('agent_notifications')
        // ->select('agent_notifications.id as id_agent_notifications', 'agent_notifications.is_read'
        //         ,'agent_notifications.id_agency_transaction as id_agency_transaction'
        //         ,'agent_notifications.topic','agent_notifications.description')
        ->where('agent_notifications.id_head_agent', $idAgency)
        ->get();
        return $notif;        
    }

    public function notifTransaction($idTransaction){
        $transaction= DB::table('agency_transactions')
            ->select('id_head_agent','id_agent','id_loan','category','withdraw','created_at as transaction_date')
            ->where('id',$idTransaction)
            ->first();
    return $transaction;
    }

    public function viewBankAccountHead($id){
        $bank= DB::table('bank_accounts')
                // ->join('m_head_agents','m_head_agents.id','bank_accounts.id_head_agent')
                // ->join('commissions','commissions.id_head_agent','m_head_agents.id')
                ->select('bank_accounts.id as id_bank_account', 'bank_accounts.bank_account_name'
                ,'bank_accounts.bank_account_number','bank_accounts.bank_name'
                ,'bank_accounts.bank_branch','bank_accounts.phone_account_number'
                // ,'commissions.commision_balance'
                )
                ->where('bank_accounts.id_head_agent', $id)
                ->first();
                
        return $bank;
    }

    public function viewBankAccount($id){
        $bank= DB::table('bank_accounts')
                // ->join('m_agents','m_agents.id','bank_accounts.id_agent')
                // ->join('commissions','commissions.id_agent','m_agents.id')
                ->select('bank_accounts.id as id_bank_account', 'bank_accounts.bank_account_name'
                ,'bank_accounts.bank_account_number','bank_accounts.bank_name'
                ,'bank_accounts.bank_branch','bank_accounts.phone_account_number'
                // ,'commissions.commision_balance'
                )
                ->where('bank_accounts.id_agent', $id)
                ->first();

        return $bank;
    }

    public function withdrawHead($idAgency, $newBalance, $withdraw){
        $commission= Commissions::where('id_head_agent', $idAgency)->first();
        $commission->commision_balance= $newBalance;
        $commission->saveOrFail();
        
        $transaction= new AgentTransactions();
        $transaction->id_head_agent= $idAgency;
        $transaction->category= "Tarik Dana";
        $transaction->withdraw= $withdraw;
        $transaction->saveOrFail();

        $notif= new AgentNotifications();
        $notif->id_head_agent= $idAgency;
        $notif->id_agency_transaction= $transaction->id;
        $notif->topic = "Komisi";
        $notif->description= "Anda telah menarik komisi";
        $transaction->saveOrFail();

        return "Berhasil update";
    }
    
    public function withdraw($idAgency, $newBalance, $withdraw){
        $commission= Commissions::where('id_agent', $idAgency)->first();
        $commission->commision_balance= $newBalance;
        $commission->saveOrFail();
        
        $transaction= new AgentTransactions();
        $transaction->id_agent= $idAgency;
        $transaction->category= "Tarik Dana";
        $transaction->withdraw= $withdraw;
        $transaction->saveOrFail();

        $notif= new AgentNotifications();
        $notif->id_agent= $idAgency;
        $notif->id_agency_transaction= $transaction->id;
        $notif->topic = "Komisi";
        $notif->description= "Anda telah menarik komisi";
        $notif->saveOrFail();

        return "Berhasil update";
    }

    // public function detailLateLoan($id_agency, $id_loan){
    //     $date = new Carbon;
    //     $installments= DB::table('loans')
    //     ->join('installments','installments.id_loan','loans.id')
    //     ->select('loans.id')
    //     ->where('loans.id_agent',$id_agency)
    //     ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    //     ->whereNull('loans.group_name')
    //     ->whereNull('installments.payment_date')
    //     ->where('installments.due_date', '<', $date)
    //     ->where('installments.id_loan', $id_loan)        
    //     ->get(); //return id_loan


    //     foreach($installments as $id_loan){
    //         $query= DB::table('loans')
    //         ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
    //         ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
    //         ->join('date_loan','date_loan.id_loan', 'loans.id')
    //         ->select('loans.id as id_loan','loans.id_agent as id_agent'
    //         ,'m_borrowers.id as id_borrower'
    //         ,'loans.loan_type','loans.loan_principal','loans.tenor'
    //         ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
    //         ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
    //         ,'d_borrowers.phone_number','d_borrowers.domicile_address'
    //         ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
    //         ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
    //         ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
    //         ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
    //         ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
    //         ,'d_borrowers.home_image','d_borrowers.salary_slip_image'
    //         ,'date_loan.submition_date'
    //         )
    //         ->where('loans.id_agent',$id)
    //         ->whereNull('loans.group_name')
    //         ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    //         ->where('loans.id',$id_loan->id)
    //         ->get();    
    //         $finalData[] = $query;
    //     }
    //     foreach($finalData as $data1){
    //         foreach($data1 as $data2){
    //             $listLate[] = $data2;
    //         }
    //     }

    //     foreach($listLate as $a){
    //         $payment= DB::table('loans')
    //         ->join('installments','installments.id_loan','loans.id')
    //         ->select('loans.id as id_loan','loans.installment_nominal','installments.due_date')
    //         ->where('loans.id_agent',$id)
    //         ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    //         ->whereNull('loans.group_name')
    //         ->whereNull('installments.payment_date')
    //         ->where('installments.due_date', '<', $date)
    //         ->where('loans.id',$a->id_loan)
    //         ->count();
    //         $a->total_late_installment= $payment;
    //         $list[] = $a;
    //     }    

    //     return $listLate;

    //     // $date = new Carbon;
    //     // $installments= DB::table('loans')
    //     // ->join('installments','installments.id_loan','loans.id')
    //     // ->select('loans.id')
    //     // ->where('loans.id_agent',$id_agency)
    //     // ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    //     // ->whereNull('loans.group_name')
    //     // ->whereNull('installments.payment_date')
    //     // ->where('installments.due_date', '<', $date)
    //     // ->groupBy('id')
    //     // ->get(); //return id_loan
    //     // dd();

    //     // foreach($installments as $id_loan){
    //     //     $query= DB::table('loans')
    //     //     ->join('m_borrowers','m_borrowers.id','loans.id_borrower')
    //     //     ->join('d_borrowers','d_borrowers.id_borrower', 'm_borrowers.id')
    //     //     ->join('date_loan','date_loan.id_loan', 'loans.id')
    //     //     ->select('loans.id as id_loan','loans.id_agent as id_agent'
    //     //     ,'m_borrowers.id as id_borrower'
    //     //     ,'loans.loan_type','loans.loan_principal','loans.tenor'
    //     //     ,'loans.installment_nominal','loans.loan_status','m_borrowers.borrower_local_id'
    //     //     ,'m_borrowers.birth_date','m_borrowers.gender','d_borrowers.name'
    //     //     ,'d_borrowers.phone_number','d_borrowers.domicile_address'
    //     //     ,'d_borrowers.rating','d_borrowers.email','d_borrowers.last_education'
    //     //     ,'d_borrowers.family_card_id','d_borrowers.principal_taxpayer_id'
    //     //     ,'d_borrowers.last_job','d_borrowers.income','d_borrowers.family_name'
    //     //     ,'d_borrowers.family_phone_number','d_borrowers.family_relationship'
    //     //     ,'d_borrowers.id_selfie_image','d_borrowers.local_id_image'
    //     //     ,'d_borrowers.family_card_image','d_borrowers.home_image','d_borrowers.salary_slip_image'
    //     //     ,'date_loan.submition_date'
    //     //     )
    //     //     ->where('loans.id_agent',$id_agency)
    //     //     ->whereNull('loans.group_name')
    //     //     ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
    //     //     ->where('loans.id',$id_loan->id)
    //     //     ->first();    
    //     //     // $finalData[] = $query;
    //     // }
    //     // return $query;

    // }
}
