<?php

namespace App\Services;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Roles;
// use Excel;

class ExportService {
/**
     * Export Data
     *
     * @param $listData
     * @param $filename
     */
    public function exportData($listData, $filename)
    {
        if ($listData) {
            foreach ($listData as $result) {
                $newArray=[];
                $newArray['Nama Admin'] = $result->admin_name;
                $newArray['Jabatan'] = $result->role_admin;
                $newArray['Tanggal Aktifitas'] = $result->created_at;
                $newArray['Keterangan Kegiatan'] = $result->description;
                $data[] = (array)$newArray;  
             }

            Excel::create($filename, function ($excel) use ($data) {
                $excel->sheet('Sheet 1', function ($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->download('csv');
        }
    }
}