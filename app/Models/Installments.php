<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Installments extends Model
{
	protected $table = 'installments';

    protected $fillable = [
    ];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loans', 'id_loan', 'id');
    }
    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrowers', 'id_borrower', 'id');
    }
}

