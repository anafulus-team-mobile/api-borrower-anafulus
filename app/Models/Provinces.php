<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Provinces extends Model
{
	protected $table = 'provinces';

    protected $fillable = [

    ];
     public function borrower()
     {
         return $this->hasMany('App\Models\DetailBorrowers', 'id_province', 'id');
     }
     public function regency()
     {
         return $this->hasMany('App\Models\Regecies', 'id_province', 'id');
     }

     public function kospin()
     {
         return $this->hasMany('App\Models\KospinLocation', 'id_province', 'id');
     }
}

