<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Regencies extends Model
{
	protected $table = 'regencies';

    protected $fillable = [
       
    ];

    // public function borrower()
    // {
    //     return $this->hasManyThrough('App\Models\Borrowers', 'App\Models\Villages', 'App\Models\SubDistricts');
    // }

      public function province()
    {
        return $this->belongsTo('App\Models\Provinces');
    }
}

