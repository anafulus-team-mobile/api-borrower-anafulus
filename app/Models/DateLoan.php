<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class DateLoan extends Model
{
	protected $table = 'date_loan';

    protected $fillable = [];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loans', 'id_loan', 'id');
    }

}

