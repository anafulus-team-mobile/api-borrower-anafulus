<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class SubDistricts extends Model
{
	protected $table = 'sub_districts';

    protected $fillable = [
    
    ];

      public function Regency()
    {
        return $this->belongsTo('App\Models\Regencies');
    }
}

