<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class BankAccounts extends Model
{
	protected $table = 'bank_accounts';

    protected $fillable = [
        
    ];

    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrowers', 'id_borrower', 'id');
    }
}

