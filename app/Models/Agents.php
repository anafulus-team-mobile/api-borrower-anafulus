<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Agents extends Model
{
	protected $table = 'm_agents';

    protected $fillable = [
        
    ];

    public function detailAgent()
    {   
        return $this->hasMany('App\Models\DetailAgents', 'id_agent', 'id');
    }

 
}
