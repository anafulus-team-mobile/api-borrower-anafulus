<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Loans extends Model
{
	protected $table = 'loans';

    protected $fillable = [

    ];

    public function groupLoan()
    {
        return $this->hasMany('App\Models\GroupLoans', 'id_loan', 'id');
    }

    public function borrower()
    {
        return $this->belongsTo('App\Models\Borrowers', 'id_borrower', 'id');
    }

    public function detailBorrower()
    {
        return $this->belongsTo('App\Models\DetailBorrowers', 'id_borrower', 'id_borrower');
    }

    public function detailAgent()
    {
        return $this->belongsTo('App\Models\DetailAgents', 'id_agent', 'id_agent');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agents', 'id_agent', 'id');
    }

    public function headAgent()
    {
        return $this->belongsTo('App\Models\HeadAgents', 'id_head_agent', 'id');
    }

    public function notification()
    {
        return $this->hasMany('App\Models\Notifications', 'id_loan', 'id');
    }
    public function installment()
    {
        return $this->hasMany('App\Models\Installments', 'id_loan', 'id');
    }

    public function dateLoan()
    {
        return $this->hasMany('App\Models\DateLoan', 'id_loan', 'id');
    }
}

