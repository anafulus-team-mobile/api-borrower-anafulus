<?php
namespace App\Helpers;

class HelpersVariable {
    public $LOAN = "Pinjaman";
    public $WITHDRAW = "Tarik Dana";
    public $DESC_CREATE_LOAN = "Berhasil mengajukan pinjaman";
    public $DESC_REJECT_LOAN = "Berhasil membatalkan pinjaman";
    public $DESC_WITHDRAW = "Berhasil melakukan penarikan dana";
    public $DESC_INSTALLMENT_SUBMISSION = "Mengajukan pembayaran cicilan";
    public $DESC_CANCEL_LOAN_SUBMISSION = "Mengajukan pembatalan pinjaman";
    public $DESC_INSTALLMENT_BY_SALDO = "Berhasil membayar cicilan dengan menggunakan saldo";
    public $DESC_CANCEL_PENALTY_BY_SALDO = "Berhasil membayar denda pembatalan pinjaman dengan menggunakan saldo";
    
}