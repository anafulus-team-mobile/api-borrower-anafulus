<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Borrowers;
use App\Models\DetailBorrowers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class AuthController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function register(Request $request)
    {     
        try{
            $mBorrower = new Borrowers();            
            $mBorrower->is_active = 1;
            $mBorrower->borrower_local_id = 0;
            $mBorrower->saveOrFail($request->all());

            $dBorrower = new DetailBorrowers();
            $dBorrower->id_borrower = $mBorrower->id;
            $dBorrower->name = $request->name; 
            $dBorrower->rating_for_lender = "B"; 
            $dBorrower->email = $request->email;             
            $dBorrower->phone_number = $request->phone_number; 
            $dBorrower->income = 0; 
            $dBorrower->family_card_id = 0;             
            $dBorrower->principal_taxpayer_id = 0; 
            $dBorrower->finance_balance = 0; 
            $dBorrower->borrower_loan = 'Belum Pernah'; 


            $dBorrower->pin = Hash::make($request->pin);
            $dBorrower->saveOrFail();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil mendaftar',
            ];    
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        } finally {
            
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function registerGoogle(Request $request)
    {     
        try{
            $mBorrower = new Borrowers();            
            $mBorrower->is_active = 1;
            $mBorrower->borrower_local_id = 0;
            $mBorrower->id_google= $request->id_google;
            $mBorrower->saveOrFail($request->all());

            $dBorrower = new DetailBorrowers();
            $dBorrower->id_borrower = $mBorrower->id;
            $dBorrower->name = $request->name; 
            $dBorrower->rating_for_lender = "B";
            $dBorrower->email = $request->email;             
            $dBorrower->phone_number = $request->phone_number; 
            $dBorrower->income = 0; 
            $dBorrower->family_card_id = 0;             
            $dBorrower->principal_taxpayer_id = 0; 
            $dBorrower->finance_balance = 0; 
            $dBorrower->borrower_loan = 'Belum Pernah'; 


            $dBorrower->pin = Hash::make($request->pin);
            $dBorrower->saveOrFail();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil mendaftar',
            ];    
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        } finally {
            
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function registerFacebook(Request $request)
    {     
        try{
            $mBorrower = new Borrowers();            
            $mBorrower->is_active = 1;
            $mBorrower->borrower_local_id = 0;
            $mBorrower->id_facebook= $request->id_facebook;
            $mBorrower->saveOrFail($request->all());

            $dBorrower = new DetailBorrowers();
            $dBorrower->id_borrower = $mBorrower->id;
            $dBorrower->name = $request->name; 
            $dBorrower->rating_for_lender = "B";
            $dBorrower->email = $request->email;             
            $dBorrower->phone_number = $request->phone_number; 
            $dBorrower->income = 0; 
            $dBorrower->family_card_id = 0;             
            $dBorrower->principal_taxpayer_id = 0; 
            $dBorrower->finance_balance = 0; 
            $dBorrower->borrower_loan = 'Belum Pernah'; 


            $dBorrower->pin = Hash::make($request->pin);
            $dBorrower->saveOrFail();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil mendaftar',
            ];    
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal mendaftar',
            ];    
        } finally {
            
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function login(Request $request)
    {
        try{
            $statusCode = 200;
            $phoneNumber=$request->phone_number;
            // $pin=$request->pin;        
            $query=DetailBorrowers::where('phone_number','=',$phoneNumber)->first();
                $response = [
                    'error' => false,
                    'message' => 'Login Berhasil',
                    'data' => [$query]
                ]; 
            } catch (Exception $ex) {
                $statusCode = 404;
                $response['message'] = 'Login Gagal';
            } finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function loginGoogle(Request $request)
    {
        try{
            $emailGoogle=$request->email;
            $query= DB::table('m_borrowers')
                    ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
                    ->where('d_borrowers.email','=',$emailGoogle)
                    ->first();
                if(!$query){
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Login Gagal',
                    ];
                }else{
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Login Berhasil',
                        'data' => [$query],
                    ]; 
                }
            } catch (Exception $ex) {
                $statusCode = 404;
                $response['message'] = 'Login Gagal';
            } finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function loginFacebook(Request $request)
    {
        try{
            $emailFacebook=$request->email;
            $query= DB::table('m_borrowers')
            ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
            ->where('d_borrowers.email','=',$emailFacebook)
            ->first();
            if(!$query){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Login Gagal',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Login Berhasil',
                    'data' => [$query],
                ]; 
            }    
            } catch (Exception $ex) {
                $statusCode = 404;
                $response['message'] = 'Login Gagal';
            } finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function checkPin(Request $request, $id)
    {
        try{
            $statusCode = 200;
            $pin=$request->confirm_pin;  
            $query=DetailBorrowers::where('id_borrower',$id)->first();
            if (Hash::check($pin, $query->pin)) {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Check Pin Berhasil',
                    'hasilnya' => [$query]
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Pin Salah',
                    'hasilnya' => [$query]
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Check Pin Gagal';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    // Forget Pin
    public function forgetPin(Request $request, $id){
        try{
                $borrower= DetailBorrowers::where('id_borrower',$id)->first();            
                $borrower->pin= Hash::make($request->new_pin);
                $borrower->saveOrFail();

                $statusCode = 201;
                $response = [
                    'error' => false,
                    'message' => 'Pin Diubah',
                ];  
        }catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Check Pin Gagal';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

// Change Pin after Login
    public function changePin(Request $request, $id)
    {
        try{
            $pin=$request->last_pin;        
            $updatePin= DetailBorrowers::where('id_borrower',$id)->first();
                if(!$updatePin)
                {
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Data Tidak Ditemukan',
                    ];
                } else if (Hash::check($pin, $updatePin->pin)){
                    $updatePin->pin = Hash::make($request->new_pin);
                    $updatePin->saveOrFail();
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Pin telah Diubah',
                    ];
                }
                else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Pin Gagal Diubah',
                    ];
                }
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Mengubah Pin',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
// Change Phone Number Before Login
    public function changePhoneNumber(Request $request)
    {
        try{
            $borrower= DB::table('m_borrowers')
                        ->join('d_borrowers','d_borrowers.id_borrower','m_borrowers.id')
                        ->select('d_borrowers.id_borrower','m_borrowers.borrower_local_id','d_borrowers.phone_number','d_borrowers.family_card_id')
                        ->where('m_borrowers.borrower_local_id', $request->borrower_local_id)
                        ->where('d_borrowers.family_card_id', $request->family_card_id)
                        ->where('d_borrowers.phone_number', $request->last_phone_number)
                        ->first();
            // dd($borrower);
                if(!$borrower)
                {
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Anda Belum Terdaftar',
                    ];
                } else{
                    $phone= DetailBorrowers::where('id_borrower',$borrower->id_borrower)->first();
                    $phone->phone_number= $request->new_phone_number;
                    $phone->saveOrFail(); 
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Berhasil',
                    ];
                }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Mengubah Nomor Ponsel',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updatePhoneNumber(Request $request, $id)
    {
        try{
            $updatePhoneNumber = DetailBorrowers::where('id_borrower', $id)->where('phone_number',$request->last_phone_number)->first();
            if(!$updatePhoneNumber)
            {
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];  
            } else{
                $updatePhoneNumber->phone_number = $request->new_phone_number;
                $updatePhoneNumber->saveOrFail();
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Nomor Ponsel Berhasil Diubah',
            ];  
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Mengubah Nomor Ponsel',
            ];    
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
}