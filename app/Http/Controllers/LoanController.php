<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loans;
use App\Models\Borrowers;
use App\Models\DateLoan;
use App\Models\DetailBorrowers;
use App\Models\DetailAgents;
use App\Models\MasterCode;
use App\Models\BorrowerNotifications;
use App\Models\AdminNotifications;
use App\Models\PaymentHistories;
use App\Models\AgentNotifications;
use Illuminate\Validation\Validator;
use App\Services\LoanService;
use DateTime;

use Illuminate\Support\Facades\DB;
use App\Models\Installments;
use App\Models\AdminFee;
use App\Models\MarginCommissions;
use App\Models\VariableFormulas;
use App\Models\AggreementDocumentations;
use App\Models\LenderPaymentTransactions;
use App\Models\FundingLoans;
use App\Models\LenderNotifications;
use App\Models\SystemCashInflow;
use App\Models\SystemFinancialAccounts;
use App\Services\BorrowerService;
use App\Helpers\HelpersVariable;

class LoanController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public $successStatus = 200;
    public function index(Request $request)
    {   
        try{
            $list= Loans::get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Daftar Pinjaman Tersedia',
                'data' => $list,
            ];
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Daftar Pinjaman',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function createLoan(Request $request)
    {
        try{
            $borrower= Borrowers::find($request->id_borrower);
            if($borrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Peminjam Tidak Ditemukan',
                ];  
            } else{
                //create log activity
                $helpersVariable= new HelpersVariable();
                $borrowerService= new BorrowerService();
                $borrowerService->createActivityBorrower($request->id_borrower, $helpersVariable->LOAN, $helpersVariable->DESC_CREATE_LOAN);
            
                $newLoan = new Loans();
                $newLoan->id_borrower = $request->id_borrower;

                if($request->status_agency == "Head Agent"){
                    $newLoan->id_head_agent = $request->id_agency;
                } else if ($request->status_agency == "Agent"){
                    $newLoan->id_agent = $request->id_agency;
                }

                $newLoan->loan_category = $request->loan_category;
                $newLoan->loan_type = $request->loan_type;
                $newLoan->loan_principal = $request->loan_principal;
                $newLoan->tenor = $request->tenor;
                $newLoan->installment_nominal = $request->installment_nominal;
                $newLoan->loan_status = "Belum Disetujui";
                $newLoan->loan_purpose = $request->loan_purpose;
                $newLoan->installment_type = $request->installment_type;
                $newLoan->saveOrFail($request->all());

                $newNotif= new AgentNotifications();
                $newNotif->id_loan = $newLoan->id;
                $newNotif->id_borrower = $request->id_borrower;

                if($request->status_agency == "Head Agent"){
                    $newNotif->id_head_agent = $request->id_agency;
                } else if ($request->status_agency == "Agent"){
                    $newNotif->id_agent = $request->id_agency;
                }
                $newNotif->description = 'Melakukan Pinjaman';
                $newNotif->saveOrFail($request->all());

                $dt = new DateTime();
                $newDateLoan= new DateLoan();
                $newDateLoan->id_loan = $newLoan->id;
                $newDateLoan->submition_date = $dt->format('Y-m-d H:i:s');
                $newDateLoan->saveOrFail($request->all());

                $updateBorrower=DetailBorrowers::where('id_borrower', $request->id_borrower)
                                ->first();
                $updateBorrower->borrower_loan = 'Sedang Berjalan';
                $updateBorrower->saveOrFail();

                $borrowerNotif= new BorrowerNotifications();
                $borrowerNotif->id_loan = $newLoan->id;
                $borrowerNotif->id_borrower = $request->id_borrower;
                if($request->status_agency == 'Head Agent'){
                    $borrowerNotif->id_head_agent = $request->id_agency;
                } else if($request->status_agency == 'Agent'){
                    $borrowerNotif->id_agent = $request->id_agency;            
                }
                $borrowerNotif->description = 'Melakukan Pinjaman Perorangan Melalui User App';
                $borrowerNotif->detail = 'Selamat ! Pinjaman Anda telah berhasil diajukan. Harap tunggu informasi selanjutnya.';
                $borrowerNotif->saveOrFail($request->all());

                $adminNotif= new AdminNotifications();
                $adminNotif->id_loan = $newLoan->id;
                $adminNotif->id_borrower = $request->id_borrower;
                if($request->status_agency == 'Head Agent'){
                    $adminNotif->id_head_agent = $request->id_agency;
                } else if($request->status_agency == 'Agent'){
                    $adminNotif->id_agent = $request->id_agency;            
                }
                $adminNotif->description = 'Melakukan Pinjaman Perorangan Melalui User App';
                $adminNotif->saveOrFail($request->all());


                // create margin commission
                // VariableFormulas
                $variableFormulas= VariableFormulas::where('category','=','Pinjaman')->first();
                //interest rate loan 
                $strLoanPercentage = explode(' ', $variableFormulas->interest_rate);
                $valueLoanPercentage = $strLoanPercentage[0] / 100 ;

                //company interest rate
                $strCompanyInterestRate = explode(' ', $variableFormulas->company_interest_rate);
                $valueCompanyInterestRate = $strCompanyInterestRate[0] / 100 ;

                $marginCommissions= new MarginCommissions();
                $marginCommissions->id_loan = $newLoan->id;
                $marginCommissions->total_week = $variableFormulas->total_week_in_year;
                $marginCommissions->total_margin_loan = $request->loan_principal * $valueLoanPercentage;
                $marginCommissions->margin_loan_percentage = $variableFormulas->interest_rate;
                $marginCommissions->margin_company_percentage = $variableFormulas->company_interest_rate;
                $marginCommissions->total_margin_company = $request->loan_principal * $valueCompanyInterestRate;
                $marginCommissions->margin_lender_percentage = $variableFormulas->lender_interest_rate;
                $marginCommissions->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Menambahkan Pinjaman',
                ];   
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menambahkan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // pinjaman dibatalkan setelah disetujui oleh admin
    public function rejectLoan(Request $request, $idBorrower)
    {
        try{
            $rejectLoan= Loans::where("id",$request->id_loan)->first();
            if(!$rejectLoan){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Pinjaman Tidak ada',
            ];
            }else{
                //create log activity
                $helpersVariable= new HelpersVariable();
                $borrowerService= new BorrowerService();
                $borrowerService->createActivityBorrower($idBorrower, $helpersVariable->LOAN, $helpersVariable->DESC_REJECT_LOAN);
                
                $rejectLoan->loan_status = "Pinjaman Dibatalkan";
                $rejectLoan->loan_penalty = $request->loan_penalty;                
                $rejectLoan->saveOrFail();

                $newNotif= new AgentNotifications();
                $newNotif->id_loan = $rejectLoan->id;
                $newNotif->id_borrower = $idBorrower;
                if($rejectLoan->id_agent === null){
                $newNotif->id_head_agent = $rejectLoan->id_head_agent;
                } else {
                    $newNotif->id_agent = $rejectLoan->id_agent;
                }
                $newNotif->description = 'Pinjaman Dibatalkan';
                $newNotif->saveOrFail($request->all());

                $updateBorrower=DetailBorrowers::where('id_borrower', $idBorrower)
                ->first();
                $updateBorrower->borrower_loan = 'Pinjaman Selesai';
                $updateBorrower->saveOrFail();

                $dt = new DateTime();
                $newDateLoan=DateLoan::where("id_loan",$request->id_loan)->first();
                $newDateLoan->id_loan = $rejectLoan->id;
                $newDateLoan->rejection_date = $dt->format('Y-m-d H:i:s');
                $newDateLoan->saveOrFail();

                $admin_fee= new AdminFee();
                $admin_fee->id_loan = $rejectLoan->id;
                $admin_fee->total_admin_fee = $request->total_admin_fee ;
                $admin_fee->admin_fee_percentage = $request->admin_fee_percentage;
                $admin_fee->description = "Pinjaman dibatalkan dan dikenai biaya administrasi";
                $admin_fee->is_charged = 1;
                $admin_fee->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Pinjaman Dibatalkan',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Melakukan Pembatalan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function installmentCategory(Request $request, $category)
    {
        try{
            $category = MasterCode::where('category', '=', $category)->get(['name']);
            $statusCode = 200;
            $response = [
                'error' => false,
                // 'message' => 'Kategori Cicilan Tersedia',
                'data' => $category,
            ];
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Kategori Cicilan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function viewByAgentCode(Request $request, $agentCode)
    {
        try {
            $agents = DetailAgents::with('agent')->where('agent_code', '=', $agentCode)
            ->select('d_agents.id_agent', 'd_agents.id_head_agent', 'd_agents.agent_code', 'd_agents.name', 'd_agents.phone_number')
            ->get();
            if($agents->isEmpty())
            {
                $statusCode = 200;
                $response = [
                    'message' => 'Agent Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Agent Tersedia',
                    'data' => $agents,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Agent',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function viewBySubdistrict(Request $request, $idSubdistrict)
    {
        try {
            $agents = DetailAgents::with('agent')->where('id_sub_district', $idSubdistrict)
            ->select('d_agents.id', 'd_agents.agent_code', 'd_agents.name', 'd_agents.phone_number')
            ->get();
            if($agents->isEmpty())
            {
                $superAgent = (object) ['id' => 11, 'name' => "Super Admin", 'agent_code' => 'SPR1', 'phone_number' => '082267573429'];
                $statusCode = 200;
                $response = [
                    'message' => 'Data super agent yang tersedia',
                    'data' => [$superAgent],

                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Agent Tersedia',
                    'data' => $agents,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Agent',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View all category that needed from Master COde
    public function filterByCategory(Request $request, $category)
    {
        try{
            $listCategory = MasterCode::where('category', '=', $category)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Tampilan Pinjaman Berdasarkan Kategori',
                'data' => $listCategory,
            ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Tampilan Kategori Pinjaman Gagal',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View all Loans based on their status
    public function filterAllByStatus (Request $request, $id)
    {
        try{   
            $loanStatus = Loans::with('dateLoan')->where('id_borrower', $id)
            ->where('loan_status','=',$request->loan_status)->get();
            if(!$loanStatus){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                    'dataLoan' => [],
                ];
            } else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Pinjaman',
                    'dataLoan' => $loanStatus,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Status Pinjaman',
            ];  
        }
        finally {
            return response(json_encode($response),$statusCode)->header('Content-Type','application/json');
        }
    }

    // View the latest Loans based on their status
    public function filterByStatus (Request $request, $id)
    {
        try{   
            $dt = new DateTime();
            $loanStatus = DB::table('loans')
            ->join('date_loan', 'date_loan.id_loan', '=', 'loans.id')
            ->where('id_borrower', $id)
            ->where('loan_status','=',$request->loan_status)
            ->select('loans.id','loans.id_borrower', 'loans.id_agent', 'loans.id_head_agent', 'loans.loan_category', 'loans.loan_type', 'loans.loan_principal', 'loans.loan_approved', 'loans.is_confirm_agent', 
                     'loans.status_paid_off','loans.is_confirm_first_admin', 'loans.loan_negotiation', 'loans.is_confirm_last_admin', 'loans.tenor', 'loans.loan_purpose', 'loans.loan_status_desc', 'loans.installment_nominal', 'loans.installment_type', 'loans.loan_penalty', 'loans.created_at', 'loans.loan_status',
                     'date_loan.submition_date', 'date_loan.validation_date', 'date_loan.cancellation_date', 'date_loan.verification_date', 'date_loan.approvement_date', 'date_loan.withdraw_fund_date')
            ->latest()
            ->first();
            if($request->loan_status == "Pinjaman Disetujui"){
                $aggreementDocumentations=AggreementDocumentations::where("id_loan",$loanStatus->id)->first();
                $loanStatus->is_signed_borrower = $aggreementDocumentations->is_signed_borrower;
                $loanStatus->link_sign_document = $aggreementDocumentations->link_sign_document;
            }

            if($request->loan_status == 'Cicilan Terlambat'){
                $installmentProggress = DB::table('installments')
                ->where('installments.id_loan', $loanStatus->id)
                ->whereNull('payment_date')
                ->where('installments.due_date','<',$dt)
                ->get();
                $loanStatus->id_installment = $installmentProggress->id;
                $loanStatus->installment_due_date = $installmentProggress->due_date;
                $loanStatus->installment_payment_date = $installmentProggress->payment_date;
                $loanStatus->installment_payment_nominal = $installmentProggress->payment_nominal;
                $loanStatus->installment_payment_type = $installmentProggress->payment_type;
            } else if($request->loan_status == 'Cicilan Sedang Berjalan'){
                $installmentProggress = DB::table('installments')
                ->where('installments.id_loan', $loanStatus->id)
                ->whereNull('payment_date')
                ->where('installments.due_date','>',$dt)
                ->first();
                $loanStatus->id_installment = $installmentProggress->id;
                $loanStatus->installment_due_date = $installmentProggress->due_date;
                $loanStatus->installment_payment_date = $installmentProggress->payment_date;
                $loanStatus->installment_payment_nominal = $installmentProggress->payment_nominal;
                $loanStatus->installment_payment_type = $installmentProggress->payment_type;
            }

            if(!$loanStatus){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                    'dataLoan' => [],
                ];
            } else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Pinjaman',
                    'dataLoan' => [$loanStatus],
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Status Pinjaman',
            ];  
        }
        finally {
            // dd($response->submition_date);
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View information of borrower's Loan based on their status
    public function loanByStatus (Request $request, $id)
    {
        try{   
            $loan = DB::table('loans')
            ->where('loans.id_borrower', $id)
            ->where('loans.id','=',$request->id_loan)
            ->where('loans.loan_status','=',$request->loan_status)
            ->join('date_loan', 'date_loan.id_loan', '=', 'loans.id')
            ->select('loans.id','loans.id_borrower', 'loans.id_agent', 'loans.loan_category', 'loans.loan_type', 'loans.loan_principal', 'loans.loan_approved', 'loans.is_confirm_agent', 
                     'loans.is_confirm_first_admin', 'loans.is_confirm_last_admin', 'loans.tenor', 'loans.loan_purpose', 'loans.loan_status_desc', 'loans.installment_nominal', 'loans.installment_type', 'loans.loan_penalty', 'loans.created_at', 'loans.loan_status',
                     'date_loan.submition_date', 'date_loan.validation_date', 'date_loan.cancellation_date', 'date_loan.rejection_date', 'date_loan.verification_date', 'date_loan.approvement_date', 'date_loan.withdraw_fund_date')
            ->first();
           if(!$loan){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                    'dataLoan' => [],
                ];
            } else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Pinjaman',
                    'dataLoan' => [$loan],
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Status Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // menampilkan informasi cicilan terlambat dan sedang berjalan
    public function installmentStatus (Request $request, $id)
    {
        try{
            $dt = new DateTime();
            $installment= DB::table('loans')
            ->join('date_loan', 'date_loan.id_loan', '=', 'loans.id')
            ->select('loans.id','loans.id_borrower', 'loans.id_agent', 'loans.loan_category',
            'loans.loan_type', 'loans.loan_principal', 'loans.loan_approved', 'loans.is_confirm_agent', 
            'loans.is_confirm_admin', 'loans.tenor', 'loans.loan_purpose', 'loans.loan_status_desc',
            'loans.installment_nominal', 'loans.installment_type', 'loans.loan_penalty', 'loans.created_at',
            'loans.loan_status')
            ->where('loans.id_borrower', $id)
            ->where('loan_status','=',$request->loan_status)
            ->where('installment_type','=',$request->installment_type)
            ->latest()
            ->first();

            if($request->loan_status == 'Cicilan Terlambat'){
                $installment->installment = DB::table('installments')
                ->where('installments.id_loan', $installment->id)
                ->whereNull('payment_date')
                ->where('installments.due_date','<',$dt)
                ->get();
            } else if($request->loan_status == 'Cicilan Sedang Berjalan'){
                $installment->installment = DB::table('installments')
                ->where('installments.id_loan', $installment->id)
                ->whereNull('payment_date')
                ->where('installments.due_date','>',$dt)
                ->first();
            }
            if(!$installment)
            {
                $statusCode = 200;
                $response = [
                    'message' => 'Pinjaman Tidak Ada',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Cicilan ',
                    'installment' => $installment,
                ];
            }               
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Menampilkan Cicilan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
        
    }

    // view loan principal that have been approved by admin and borrower
    public function viewLoanPrincipal(Request $request, $id){
        try{
            $loan= Loans::where('id_borrower', $id)
            ->where('loan_status','=','Pinjaman Disetujui')->latest()->first();
            if(!$loan)
            {
                $statusCode = 200;
                $response = [
                    'message' => 'Pinjaman Tidak Ada',
                ];
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Total Pinjaman Tersedia',
                    'loan_principal' => $loan->loan_principal,
                ];
            }           
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Gagal Menampilkan Saldo',
                    ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateBalance(Request $request, $id)
    {
        try{
            //create log activity
            $helpersVariable= new HelpersVariable();
            $borrowerService= new BorrowerService();
            $borrowerService->createActivityBorrower($id, $helpersVariable->WITHDRAW, $helpersVariable->DESC_WITHDRAW);
            
            $updateLoan = Loans::where('id_borrower', $id)->first();
            $updateLoan->loan_principal = $request->loan_principal;
            $updateLoan->saveOrFail();
            $updateBorrower = DetailBorrowers::where('id_borrower', $id)->first();
            $updateBorrower->finance_balance = $request->finance_balance;
            $updateBorrower->saveOrFail();
            $statusCode = 200;
            $response = [
            'error' => true,
            'message' => 'Update Saldo',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Gagal Update Saldo',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View the Loans status
    public function viewLoanStatus (Request $request, $idBorrower)
    {
        try{
            $loan= Loans::where('id_borrower', $idBorrower)
                        ->where('id',$request->id_loan)
                        ->latest()
                        ->first();
            $statusCode = 200;
            $response = [
                'dataLoan' => [$loan],
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Gagal Menampilkan Saldo',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
        
    public function createPaymentLoanHistory (Request $request, $idBorrower)
    {
        try{
            $dt = new DateTime();
            $modify = $dt->modify('+1 day');
            
            $newPaymentLoans= new PaymentHistories();
            $newPaymentLoans->id_borrower = $idBorrower;
            $newPaymentLoans->id_loan = $request->id_loan;
            $newPaymentLoans->virtual_account_number = $request->virtual_account_number;
            if($request->category == 'Cicilan Sedang Berjalan'){
                //create log activity
                $helpersVariable= new HelpersVariable();
                $borrowerService= new BorrowerService();
                $borrowerService->createActivityBorrower($idBorrower, $helpersVariable->LOAN, $helpersVariable->DESC_INSTALLMENT_SUBMISSION);

                $newPaymentLoans->id_installment = $request->id_installment;
                $newPaymentLoans->installment_paid_off = $request->installment_paid_off;
                $newPaymentLoans->category = $request->category;
            } else if($request->category == 'Pinjaman Dibatalkan'){
                //create log activity
                $helpersVariable= new HelpersVariable();
                $borrowerService= new BorrowerService();
                $borrowerService->createActivityBorrower($idBorrower, $helpersVariable->LOAN, $helpersVariable->DESC_CANCEL_LOAN_SUBMISSION);
                
                $newPaymentLoans->category = $request->category;
            }
            $newPaymentLoans->payment_due_date = $modify->format('Y-m-d H:i:s');
            $newPaymentLoans->total_payment = $request->total_payment;
            $newPaymentLoans->payment_type = $request->payment_type;
            $newPaymentLoans->order_id = $request->order_id;
            $newPaymentLoans->saveOrFail($request->all());
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Payment Loan Berhasil Ditambahkan',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal menambahkan Payment Loan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // View the Loans status
    public function getPaymentHistory (Request $request, $idBorrower)
    {
        try{
            $paymentHistory= DB::table('payment_histories')
                        ->join('loans', 'loans.id', '=', 'payment_histories.id_loan')
                        ->select('payment_histories.id_loan','payment_histories.id_borrower'
                        // , 'payment_histories.id_virtuall_account'
                        ,'payment_histories.virtual_account_number'
                        ,'payment_histories.is_payment','payment_histories.category'
                        ,'payment_histories.payment_type','payment_histories.total_payment'
                        ,'payment_histories.installment_paid_off'
                        ,'payment_histories.payment_due_date', 'payment_histories.created_at',
                        'loans.loan_principal', 'loans.loan_penalty')                
                        ->where('payment_histories.id_borrower', $idBorrower)
                        ->whereNull('payment_histories.is_payment')
                        ->latest()
                        ->first();
            $statusCode = 200;

            if(!$paymentHistory)
            {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Menampilkan History pembayaran',    
                    'dataPaymentHistory' => $paymentHistory,
                ];
    
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Menampilkan History pembayaran',    
                    'dataPaymentHistory' => [$paymentHistory],
                ];
                }           

        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Gagal Menampilkan History pembayaran',    
           ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }    

    //dipakai
    public function cancelLoanBeforeLoanApprove($idLoan){
        try{
            $rejectLoan= Loans::where("id",$idLoan)->first();
            if(!$rejectLoan){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Pinjaman Tidak ada',
            ];
            }else{
                $rejectLoan->loan_status = "Pinjaman Dibatalkan";
                $rejectLoan->saveOrFail();
                //create log activity
                $helpersVariable= new HelpersVariable();
                $borrowerService= new BorrowerService();
                $borrowerService->createActivityBorrower($rejectLoan->id_borrower, $helpersVariable->LOAN, $helpersVariable->DESC_REJECT_LOAN);

                $newNotif= new AgentNotifications();
                $newNotif->id_loan = $idLoan;
                $newNotif->id_borrower = $rejectLoan->id_borrower;
                if($rejectLoan->id_head_agent===null){
                    $newNotif->id_agent = $rejectLoan->id_agent;                
                } else {
                    $newNotif->id_head_agent = $rejectLoan->id_head_agent;
                }    
                $newNotif->description = 'Pinjaman Dibatalkan';
                $newNotif->saveOrFail();

                $borrowerNotif= new BorrowerNotifications();
                $borrowerNotif->id_loan = $idLoan;
                $borrowerNotif->id_borrower = $rejectLoan->id_borrower;
                if($rejectLoan->id_head_agent===null){
                    $borrowerNotif->id_agent = $rejectLoan->id_agent;                
                } else {
                    $borrowerNotif->id_head_agent = $rejectLoan->id_head_agent;
                }    
                $borrowerNotif->description = 'Anda Membatalkan Pinjaman';
                $borrowerNotif->detail = 'Anda telah membatalkan Pinjaman. Proses pengajuan pinjaman Anda tidak dapat dilanjutkan lagi.';
                $borrowerNotif->saveOrFail();

                $updateBorrower=DetailBorrowers::where('id_borrower', $rejectLoan->id_borrower)
                ->first();
                $updateBorrower->borrower_loan = 'Pinjaman Selesai';
                $updateBorrower->saveOrFail();

                $dt = new DateTime();
                $dateLoan= DateLoan::where('id_loan', $idLoan)->first();
                $dateLoan->cancellation_date= $dt->format('Y-m-d H:i:s');
                $dateLoan->saveOrFail();

                $admin_fee= new AdminFee();
                $admin_fee->id_loan = $idLoan;
                $admin_fee->total_admin_fee = 0 ;
                $admin_fee->admin_fee_percentage = "-";
                $admin_fee->description = "Pinjaman dibatalkan dan tidak dikenai biaya administrasi";
                $admin_fee->is_charged = 0;
                $admin_fee->saveOrFail();
        

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Pinjaman Dibatalkan',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Melakukan Pembatalan Pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }    

        //dipakai
        public function payWithSaldo(Request $request){
            try{                
                // //update saldo
                $updateBorrower = DetailBorrowers::where('id_borrower', $request->id_borrower)->first();
                $updateBorrower->finance_balance = $request->update_finance_balance;
                $updateBorrower->saveOrFail();
    
                // // update status cicilan
                $dt = new DateTime();
                $updateInstallment = Installments::where('id', $request->id_installment)->first();
                $updateInstallment->payment_nominal = $request->installment;
                $updateInstallment->payment_date = $dt->format('Y-m-d H:i:s');
                $updateInstallment->payment_type = "Menggunakan Saldo";
                $updateInstallment->saveOrFail();
    
                //create log activity
                $helpersVariable= new HelpersVariable();
                $borrowerService= new BorrowerService();
                $borrowerService->createActivityBorrower($request->id_borrower, $helpersVariable->LOAN, $helpersVariable->DESC_INSTALLMENT_BY_SALDO);
                
                $newNotif= new BorrowerNotifications();
                $newNotif->id_loan = $request->id_loan;
                $newNotif->id_borrower = $request->id_borrower;
                $newNotif->id_installment = $request->id_installment;            
                $newNotif->description = 'Berhasil membayar cicilan menggunakan saldo anda';
                $newNotif->detail = 'Pembayaran cicilan Anda telah berhasil dilakukan. Jumlah saldo Anda telah terpotong secara otomatis sesuai dengan jumlah cicilan yang Anda bayarkan.';
                $newNotif->saveOrFail();

                // paymentLoanHistory
                $newPaymentLoans= new PaymentHistories();
                $newPaymentLoans->id_borrower = $request->id_borrower;
                $newPaymentLoans->id_loan = $request->id_loan;
                $newPaymentLoans->is_payment = 1;
                $newPaymentLoans->payment_type = "Menggunakan Saldo";                
                $newPaymentLoans->id_installment = $request->id_installment;
                $newPaymentLoans->installment_paid_off = $request->installment_paid_off;
                $newPaymentLoans->category = "Cicilan Sedang Berjalan";
                $newPaymentLoans->payment_due_date = $dt->format('Y-m-d H:i:s');
                $newPaymentLoans->total_payment = $request->installment;
                $newPaymentLoans->saveOrFail();

                // cash in and finance account
                $cashIn= new SystemCashInflow();
                $cashIn->id_borrower = $request->id_borrower;
                $cashIn->cash_amount = $request->installment;
                $cashIn->category = "Cicilan Pinjaman";
                $cashIn->payment_type = "Menggunakan Saldo";
                $cashIn->saveOrFail();
    
                $getLastFinanceAccount = SystemFinancialAccounts::latest()->first();
                $financeAccount= new SystemFinancialAccounts();
                $financeAccount->id_system_cash_inflow = $cashIn->id;
                if($getLastFinanceAccount){
                    $newFinanceBalance = $getLastFinanceAccount->last_finance_balance + $request->installment; 
                    $financeAccount->last_finance_balance = $getLastFinanceAccount->last_finance_balance;
                    $financeAccount->new_finance_balance = $newFinanceBalance;    
                } else {
                    $newFinanceBalance = 0 + $request->installment; 
                    $financeAccount->last_finance_balance = 0;
                    $financeAccount->new_finance_balance = $newFinanceBalance;
                }
                $financeAccount->saveOrFail();
                $updateLoan = Loans::where('id', $request->id_loan)->first();

                // add commission lender, cash out, and finance account
                $loanService = new LoanService();
                if($getLastFinanceAccount){
                    $loanService->addLenderCommission($updateLoan->list_id_lender, $getLastFinanceAccount->last_finance_balance, $newFinanceBalance, $request->installment_paid_off, $request->id_loan);
                } else {
                    $loanService->addLenderCommission($updateLoan->list_id_lender, 0, $newFinanceBalance, $request->installment_paid_off, $request->id_loan);
                }

                
                // check cicilan ke berapa, kalau terakhir pinjaman selesai
                $checkInstallment = Installments::where('id_borrower', $request->id_borrower)                    
                ->whereNull('payment_date')
                ->count();

                if($checkInstallment === 0){

                    $updateBorrower = DetailBorrowers::where('id_borrower', $request->id_borrower)->first();
                    $updateBorrower->borrower_loan = "Pinjaman Selesai";
                    $updateBorrower->saveOrFail();

                    $updateLoan->loan_status = "Pinjaman Selesai";
                    $updateLoan->saveOrFail();

                    $newNotif= new BorrowerNotifications();
                    $newNotif->id_loan = $request->id_loan;
                    $newNotif->id_borrower = $request->id_borrower;
                    $newNotif->description = 'Pinjaman Anda sudah lunas';
                    $newNotif->detail = 'Selamat ! Anda telah berhasil melunasi pinjaman Anda. Kini, Anda dapat melakukan pinjaman baru.';
                    $newNotif->saveOrFail();
                }
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Update Saldo',
                ];
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Update Saldo',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }
    
        public function detailInstallmentStatus ($id, Request $request)
        {
            try{
                $dt = new DateTime();
                $installment= DB::table('loans')
                ->where('loans.id', $id)
                ->first();
    
                if($request->status_installment == 'Cicilan Terlambat'){
                    $installment->installment = DB::table('installments')
                    ->where('installments.id_loan', $installment->id)
                    ->whereNull('payment_date')
                    ->where('installments.due_date','<',$dt)
                    ->get();
    
                    $installmentDateLoan = DB::table('date_loan')
                    ->where('id_loan', $installment->id)
                    ->first();
    
                    $installment->id_installment = $installmentProggress->id;
                    $installment->installment_due_date = $installmentProggress->due_date;
                    $installment->installment_payment_date = $installmentProggress->payment_date;
                    $installment->installment_payment_nominal = $installmentProggress->payment_nominal;
                    $installment->installment_payment_type = $installmentProggress->payment_type;
    
    
                    //installmentDate                
                    $installment->submition_date = $installmentDateLoan->submition_date;        
                    $installment->validation_date = $installmentDateLoan->validation_date;        
                    $installment->verification_date = $installmentDateLoan->verification_date;        
                    $installment->approvement_date = $installmentDateLoan->approvement_date;        
                    $installment->withdraw_fund_date = $installmentDateLoan->withdraw_fund_date;        
    
                } else if($request->status_installment == 'Cicilan Sedang Berjalan'){
                    $installmentProggress = DB::table('installments')
                    ->where('installments.id_loan', $installment->id)
                    ->whereNull('payment_date')
                    ->where('installments.due_date','>',$dt)
                    ->first();
    
                    $installmentDateLoan = DB::table('date_loan')
                    ->where('id_loan', $installment->id)
                    ->first();
    
                    $paidOffInstallment= DB::table('loans')
                    ->join('installments','installments.id_loan','loans.id')
                    ->where('loans.loan_status','=','Cicilan Sedang Berjalan')
                    ->whereNull('loans.group_name')
                    ->whereNotNull('installments.payment_date')
                    ->where('loans.id', $id)
                    ->count();
        
                    $installment->id_installment = $installmentProggress->id;
                    $installment->installment_due_date = $installmentProggress->due_date;
                    $installment->installment_payment_date = $installmentProggress->payment_date;
                    $installment->installment_payment_nominal = $installmentProggress->payment_nominal;
                    $installment->installment_payment_type = $installmentProggress->payment_type;
                    $installment->paid_off_installment = $paidOffInstallment;        
                    
                    //installmentDate                
                    $installment->submition_date = $installmentDateLoan->submition_date;        
                    $installment->validation_date = $installmentDateLoan->validation_date;        
                    $installment->verification_date = $installmentDateLoan->verification_date;        
                    $installment->approvement_date = $installmentDateLoan->approvement_date;        
                    $installment->withdraw_fund_date = $installmentDateLoan->withdraw_fund_date;        
                    
                }
                if(!$installment)
                {
                    $statusCode = 200;
                    $response = [
                        'message' => 'Pinjaman Tidak Ada',
                    ];
                } else {
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Cicilan ',
                        'dataLoan' => [$installment],
                    ];
                }               
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Gagal Menampilkan Cicilan',
                ];  
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
        }
    
    //dipakai
    public function approveLoanNegotiation($idLoan)
    {
        try{
            $updateLoan = Loans::where('id', $idLoan)->first();
            $updateLoan->loan_status = "Pinjaman Disetujui";
            $updateLoan->loan_negotiation = "Negosiasi Selesai";
            $updateLoan->loan_principal = $updateLoan->loan_approved;
            $updateLoan->saveOrFail();

            $dt = new DateTime();
            $newDateLoan= DateLoan::where('id_loan', $idLoan)->first();
            $newDateLoan->approvement_date = $dt->format('Y-m-d H:i:s');
            $newDateLoan->saveOrFail();

            $statusCode = 200;
            $response = [
            'error' => true,
            'message' => 'Update Pinjaman',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
            'error' => true,
            'message' => 'Gagal Update status pinjaman',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getMasterCode(Request $request)
    {
        try{
            $category= MasterCode::where('category','=', $request->creategory)
                        ->first();
            if(!$category){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Master Code',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Tampilkan Master Code',
                    'dataMasterCode' => [$category],
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
}
