<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\VariableFormulas;
use App\Models\Borrowers;
use App\Models\DetailBorrowers;
use App\Models\Loans;
use App\Models\VirtualAccounts;
use App\Models\BankAccounts;
use App\Models\Helps;
use App\Models\Questions;
use App\Models\BorrowerNotifications;
use App\Models\Villages;
use App\Models\Provinces;
use App\Models\Regencies;
use App\Models\SubDistricts;
use App\Models\PaymentPartners;
use App\Services\BorrowerService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

class BorrowerController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public $successStatus = 200;
    protected $image_folder = '/borrower-file/';
    protected $image_profile_folder = '/image/';

    public function getVariableFormula(Request $request, $category)
    {
        try {
            $variable= VariableFormulas::where('category', '=', $category)->first();
            if(!$variable)
            {   $statusCode = 200;
                $response = [
                    'message' => 'Variable formula Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Variable formula ditampilkan',
                    'dataVariable' => [$variable],
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Variable',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updatePersonalData(Request $request, $id)
    {
        try{
            $updateBorrower = Borrowers::find($id);

            if($updateBorrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data Tidak Ditemukan',
                ];  
            } else{
                $updateBorrower->borrower_local_id = $request->borrower_local_id;
                $updateBorrower->birth_date = $request->birth_date;
                $updateBorrower->gender = $request->gender;
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $id)->first();                
                $updateDetailBorrower->name = $request->name;
                $updateDetailBorrower->email = $request->email;
                $updateDetailBorrower->phone_number = $request->phone_number;
                $updateDetailBorrower->last_education = $request->last_education;
                $updateDetailBorrower->last_job = $request->last_job;
                $updateDetailBorrower->income = $request->income;
                $updateDetailBorrower->family_card_id = $request->family_card_id;
                $updateDetailBorrower->principal_taxpayer_id = $request->principal_taxpayer_id;
                $updateBorrower->saveOrFail();                         
                $updateDetailBorrower->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Berhasil Update Data Pribadi'
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Update Data Probadi',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateAddress(Request $request, $id)
    {
        try{
            $updateBorrower = Borrowers::find($id);
            if($updateBorrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Data Tidak Ditemukan',
                ];  
            } else{
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $id)->first();                
                $updateDetailBorrower->id_village = $request->id_village;
                $updateDetailBorrower->domicile_address = $request->domicile_address;
                $updateDetailBorrower->id_card_address = $request->id_card_address;
                $updateBorrower->saveOrFail();                         
                $updateDetailBorrower->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Berhasil Update Alamat',
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Update Alamat',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updatePhotoProfile(Request $request, $id)
    {
        try{
            $updateBorrower = Borrowers::find($id);
            if($updateBorrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                ];  
            } else{
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $id)->first();                
            if ($request->hasFile('profil_image')) {
                if ($request->file('profil_image')->isValid()) {
                    $file_ext        = $request->file('profil_image')->getClientOriginalExtension();
                    $file_size       = $request->file('profil_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_profile_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('profil_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $id_profil_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('profil_image')->move($dest_path, $id_profil_image_name);
                        $updateDetailBorrower->profil_image= $id_profil_image_name;
                    }
                }
            }
                $updateBorrower->saveOrFail();                         
                $updateDetailBorrower->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Update Data Persyaratan',
                    
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Data Persyaratan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }    
    }
    public function updateRequirement(Request $request, $id)
    {
        try{
            $updateBorrower = Borrowers::find($id);
            if($updateBorrower->get()->isEmpty())
            {   
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ditemukan',
                ];  
            } else{
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $id)->first();                
                $updateDetailBorrower->family_phone_number = $request->family_phone_number;
                $updateDetailBorrower->family_relationship = $request->family_relationship;
                $updateDetailBorrower->family_name = $request->family_name;
            if ($request->hasFile('id_selfie_image')) {
                if ($request->file('id_selfie_image')->isValid()) {
                    $file_ext        = $request->file('id_selfie_image')->getClientOriginalExtension();
                    $file_size       = $request->file('id_selfie_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('id_selfie_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $id_selfie_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('id_selfie_image')->move($dest_path, $id_selfie_image_name);
                        $updateDetailBorrower->id_selfie_image= $id_selfie_image_name;
                    }
                }
            }
            if ($request->hasFile('home_image')) {
                if ($request->file('home_image')->isValid()) {
                    $file_ext        = $request->file('home_image')->getClientOriginalExtension();
                    $file_size       = $request->file('home_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('home_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $home_image_name = $file_name . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('home_image')->move($dest_path, $home_image_name);
                        $updateDetailBorrower->home_image= $home_image_name;
                        // $updateDetailBorrower->home_image = $request->home_image;
                    }
                }
            }
            if ($request->hasFile('local_id_image')) {
                if ($request->file('local_id_image')->isValid()) {
                    $file_ext        = $request->file('local_id_image')->getClientOriginalExtension();
                    $file_size       = $request->file('local_id_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('local_id_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $local_id_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('local_id_image')->move($dest_path, $local_id_image_name);
                        $updateDetailBorrower->local_id_image= $local_id_image_name;
                        // $updateDetailBorrower->local_id_image = $request->local_id_image;
                    }
                }
            }
            if ($request->hasFile('salary_slip_image')) {
                if ($request->file('salary_slip_image')->isValid()) {
                    $file_ext        = $request->file('salary_slip_image')->getClientOriginalExtension();
                    $file_size       = $request->file('salary_slip_image')->getClientSize();
                    $allow_file_exts = array('jpeg', 'jpg', 'png');
                    $max_file_size   = 1024 * 1024 * 10;
                    if (in_array(strtolower($file_ext), $allow_file_exts) && ($file_size <= $max_file_size)) {
                        $dest_path     = public_path() . $this->image_folder;
                        $file_name     = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('salary_slip_image')->getClientOriginalName());
                        $file_name     = str_replace(' ', '-', $file_name);
                        $salary_slip_image_name = $file_name  . '.' . $file_ext;
    
                        // move file to serve directory
                        $request->file('salary_slip_image')->move($dest_path, $salary_slip_image_name);
                        $updateDetailBorrower->salary_slip_image= $salary_slip_image_name;
                    }
                }
            }
                $updateBorrower->saveOrFail();                         
                $updateDetailBorrower->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Berhasil Update Data Persyaratan',
                    
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Data Persyaratan',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    // View Balance of Borrower by Borrower id
    public function viewBalance(Request $request, $id)
    {
        try{
            $balance= DetailBorrowers::where('id_borrower', $id)->get(['finance_balance']);                
            $statusCode = 200;
            $response = [
                'error' => true,
                'message' => 'Saldo Ditampilkan',
                'data' => $balance,
            ];
        
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Saldo',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
  
    public function deleteBankAccount(Request $request, $id)
    {
        $deleteBankAccount = BankAccounts::find($id);
        if(!$deleteBankAccount){
            abort(404);
        } else{
            $deleteBankAccount->delete();
            return "success";
        }
    }
    
    public function editBankAccount(Request $request, $id)
    {
        try{
            $updateBankAccount= BankAccounts::find($id);
            $updateBankAccount->bank_account_number = $request->bank_account_number; 
            $updateBankAccount->bank_account_name = $request->bank_account_name; 
            $updateBankAccount->bank_name = $request->bank_name; 
            $updateBankAccount->bank_branch = $request->bank_branch;
            $updateBankAccount->saveOrFail();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Bank Account Berhasil Diupdate',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function createBankAccount(Request $request, $id)
    {
        try{
            $newBankAccount= new BankAccounts();
            $newBankAccount->id_borrower = $id;
            $newBankAccount->bank_account_number = $request->bank_account_number; 
            $newBankAccount->bank_account_name = $request->bank_account_name; 
            $newBankAccount->bank_name = $request->bank_name; 
            $newBankAccount->bank_branch = $request->bank_branch;
            $newBankAccount->saveOrFail();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Bank Account Berhasil Ditambahkan',
            ];
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    public function viewDetailBankAccount(Request $request, $id)
    {
        try{
            $detailBankAccount= BankAccounts::where('id',$id)->first();                 
            $statusCode = 200;
            $response = [
                'error' => true,
                'message' => 'Bank Account Ditampilkan',
                'dataBankAccount' => [$detailBankAccount],
            ];
        
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Bank Account',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    // Update data rekening Bank berdasarkan nama Bank si peminjam
    public function updateBankAccount(Request $request, $id)
    {
        try{
            $bankAcc= BankAccounts::where('id_borrower',$id)
            ->where('bank_name','=',$request->bank_name)->first();
            if(!$bankAcc)
            {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Tersedia',
                ];
            } else {
                $bankAcc->phone_account_number = $request->phone_account_number;
                $bankAcc->bank_account_number = $request->bank_account_number;
                $bankAcc->bank_account_name = $request->bank_account_name;
                $bankAcc->bank_name = $request->bank_name;
                $bankAcc->bank_branch = $request->bank_branch;
                $bankAcc->saveOrFail($request->all());
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Update Rekening Bank',
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Update Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewBankAccount(Request $request, $id)
    {
        try {
            $view= BankAccounts::where('id_borrower',$id)->get();
        if($view->isEmpty())
        {   $statusCode = 200;
            $response = [
                'message' => 'Rekening Bank Belum Tercatat',
            ];
        }else{
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Rekening Bank Ditampilkan',
                'dataBankAccount' => $view,
            ];
        }
      
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Rekening Bank',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }        
    }

    public function viewInstallmentNominal(Request $request, $id)
    {
        try{
            $installment= Loans::with('installment')->where('id_borrower', $id)->latest()
            ->first();
            if(!$installment)
            {   $statusCode = 200;
                $response = [
                    'message' => 'Anda Tidak Memiliki Cicilan',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Cicilan sebesar',
                    'installment_nominal' => $installment->installment_nominal,
                ];
                }
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Jumlah Cicilan',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }     
    }

    public function detailVirtualAccount(Request $request, $id)
    {
        try{
            // $dataVirtual= VirtualAccounts::find($id)->first();            
            $virtualAccount= VirtualAccounts::with('question')->with('help')
            ->where('id',$id)->get();
            if(!$virtualAccount)
            {   $statusCode = 200;
                $response = [
                    'message' => 'Tidak Terdapat Data Virtual Account',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Virtual Account',
                    'data' => $virtualAccount,
                ];
                }
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Data Virtual Account',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function viewVirtualAccount(Request $request)
    {
        try{
            $virtual= VirtualAccounts::get();
            $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Daftar Virtual Account',
                    'data' => $virtual,
                ];
        }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Data Virtual Account',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    public function viewHelp(Request $request, $id)
    {
        try{
            $question= Questions::where('id',$id)->first(); 
            $answer= Helps::with('question')->where('id_question',$id)->get();
            if(!$answer)
            {   $statusCode = 200;
                $response = [
                    'message' => 'Tidak Terdapat Bantuan',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Help Page',
                    'Question' => $question->question,
                    'data' => $answer,
                ];
                }
            }catch (Exception $ex) {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Gagal Tampilkan Help Page',
                ];
            }
            finally {
                return response($response,$statusCode)->header('Content-Type','application/json');
            }
    }

    // public function createQuestion(Request $request)
    // {
    //     try{
    //         $borrower = Borrowers::find($request->id_borrower);
    //         if($borrower->get()->isEmpty())
    //         {   
    //             $statusCode = 404;
    //             $response = [
    //                 'error' => false,
    //                 'message' => 'Data Tidak Ditemukan',
    //             ];  
    //         } else{
    //             $newQuestion = new Questions();
    //             $newQuestion->id_borrower = $request->id_borrower;
    //             $newQuestion->question = $request->question;
    //             $newQuestion->save();
    //             $statusCode = 200;
    //             $response = [
    //                 'error'=> false,
    //                 'message' => 'Pertanyaan Ditambahkan',
    //                 // 'Pertanyaan' => $newQuestion->question,
    //             ];
    //         }
    //     } catch (Exception $ex) {
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Tambahkan Pertanyaan',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    // }

    function viewRating(Request $request, $id)
     {  
        try{
         $rating= DetailBorrowers::with('borrower')->where('id_borrower',$id)->first(['rating']);
         if(!$rating){
            $statusCode = 404;
            $response = [
                'error' => true,
               'message' => 'Data Tidak Ada',
           ];
         } else{
            $statusCode = 200;
            $response = [
               'error' => false,
               'message' => 'Rating Borrower Ditampilkan',
               'data' => $rating->rating,
           ];
         }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Rating Borrower',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // update notifikasi is read untuk Pinjaman yang diajukan Borrower kepada Agent
    public function updateReadNotification ($id_notification)
    {
        try{
            $notification = BorrowerNotifications::where('id',$id_notification)
            // ->where('id_borrower', $id)
            ->first();
            if(!$notification){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Data Tidak Ada',
                ];
            }else{
                $notification->is_read = 1;
                $notification->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Notifikasi Telah Dibaca',
// /                    'data' => $notification,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Rating Borrower',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // update notifikasi is read untuk Pinjaman yang diajukan Borrower kepada Head Agent
    // public function updateReadNotificationHead (Request $request, $id)
    // {
    //     try{
    //         $notification = Notifications::where('id_borrower', $id)
    //         ->where('id_loan', $request->id_loan)
    //         ->Where('id_head_agent', $request->id_head_agent)
    //         ->first();
    //         if(!$notification){
    //             $statusCode = 404;
    //             $response = [
    //                 'error' => true,
    //                 'message' => 'Data Tidak Ada',
    //             ];
    //         }else{
    //             $notification->is_read = $request->is_read;
    //             $notification->saveOrFail();
    //             $statusCode = 200;
    //             $response = [
    //                 'error' => false,
    //                 'message' => 'Notifikasi Telah Dibaca',
    //                 'data' => $notification,
    //             ];
    //         }
    //     }catch (Exception $ex) {
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Tampilkan Rating Borrower',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    // }

    
    public function viewVillage(Request $request, $idSubdistrict){
        try {
            $list= Villages::where('id_sub_district', $idSubdistrict)->get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Kelurahan Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Kelurahan Ditampilkan',
                    'dataVillages' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Kelurahan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewSubdistrics(Request $request, $idRegence){
        try {
            $list= SubDistricts::where('id_regency', $idRegence)->get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Kelurahan Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Kelurahan Ditampilkan',
                    'dataSubdistrics' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Kelurahan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function viewRegency(Request $request, $idProvince){
        try {
            $list= Regencies::where('id_province', $idProvince)->get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Kota Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Kota Ditampilkan',
                    'dataRegencies' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Kota',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewProvince(Request $request)
    {
        try {
            $list= Provinces::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Provinci Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Provinci Ditampilkan',
                    'dataProvinces' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Provinsi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateFinanceBalance(Request $request, $id)
    {
        try{
            $loan = Loans::where('id_borrower', $id)->latest()->first();
            $updateBorrower = Borrowers::find($id);
            if($loan->is_withdraw == 1){
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $id)->first();
                $updateDetailBorrower->finance_balance = $request->finance_balance;
                $updateDetailBorrower->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Berhasil Update Saldo',
                ];    
            } else {
                $updateDetailBorrower = DetailBorrowers::where('id_borrower', $id)->first();                
                $updateDetailBorrower->finance_balance = $request->finance_balance;
                $loan->is_withdraw = 1;                
                $updateDetailBorrower->saveOrFail();
                $loan->saveOrFail();
                $statusCode = 200;
                $response = [
                    'error' => true,
                    'message' => 'Berhasil Update Saldo',
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => false,
                'message' => 'Gagal Update Saldo',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getPaymentPartner(Request $request)
    {
        try{
            $listBankPartners = PaymentPartners::where('payment_type',$request->payment_type)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Menapilkan list partner bank',
                'dataPaymentPartner' => $listBankPartners,
            ];  
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal menapilkan list partner bank',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function regulerLoan($id_loan){
        try{
            $borrowerService= new BorrowerService();
            $loan= $borrowerService->detailLoan($id_loan);
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Menapilkan list partner bank',
                'dataPaymentPartner' => $loan,
            ];  
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal menapilkan list partner bank',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function installmentLoan(Request $request, $id_loan){
        try{
            $borrowerService= new BorrowerService();
            if($request->loan_status == 'Cicilan Sedang Berjalan'){
                $loan= $borrowerService->installmentLoan($id_loan);
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'detail Cicilan Sedang Berjalan ',
                    'dataPaymentPartner' => $loan,
                ];
            } else if($request->loan_status == 'Cicilan Terlambat'){
                $loan= $borrowerService->installmentLateLoan($id_loan);
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'detail Cicilan Terlambat',
                    'dataPaymentPartner' => $loan,
                ];
            } else {
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'data Tidak Ada',
                ];
            }
              
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal menapilkan list partner bank',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function withdrawLoan(Request $request, $id_borrower){
        try{
            $service= new BorrowerService();
                $withdraw= $service->withdrawLoan($id_borrower, $request->updated_balance, $request->tenor, $request->total_admin_fee, $request->admin_fee_percentage);
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Dana Pinjaman berhasil ditarik ke Saldo',
                ];
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal menapilkan list partner bank',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getAllBorrowers()
    {
        try {
            $list= DetailBorrowers::get();
            if($list->isEmpty())
            {   $statusCode = 200;
                $response = [
                    'message' => 'Data Peminjam Belum ada',
                ];
            }else{
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Peminjam',
                    'data' => $list,
                ];
            }
        }catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Tampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    
}