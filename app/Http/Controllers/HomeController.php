<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promotions;
use App\Models\Loans;
use App\Models\Borrowers;
use App\Models\MasterCode;
use App\Models\Installments;
use App\Models\DetailBorrowers;
use App\Models\CompanyInformations;
use App\Models\BorrowerNotifications;
use App\Services\BorrowerService;
use App\Services\LoanService;
use App\Models\PaymentHistories;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\AggreementDocumentations;

class HomeController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public $successStatus = 200;

    public function viewPromotion(Request $request)
    {   
        try{
            $images= Promotions::where('is_active', 1)->get();
            if(!$images){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Foto Promosi Tidak Tersedia',
                ];
            } else {
                $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Foto Promosi Tersedia',
                'dataPromotions' => $images,
            ];
            }
            
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Foto Promosi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
    
    public function viewProfile(Request $request, $id)
    {   
        try{
            $borrowerService = new BorrowerService();
            $query       = $borrowerService->viewProfileBorrower($id);

            if(!$query){
                $statusCode = 200;
                $response = [
                        'error' => false,
                        'message' => 'Data Tidak Ada',
                ];
            } else {
            $statusCode = 200;
                $response = [
                        'error' => false,
                        'message' => 'Tampilkan Data Peminjam',
                        'data' => [$query],
                ];
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Data Peminjam',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
        
    }
    public function viewLoanStatus(Request $request, $id)
    {   
        try{
            $loanservice= new LoanService();
            $loan= Loans::where('id_borrower',$id)
                        ->latest()->first();
            
            if($loan->loan_status == "Pinjaman Disetujui"){
                $aggreementDocumentations=AggreementDocumentations::where("id_loan",$loan->id)->first();
                $loan->is_signed_borrower = $aggreementDocumentations->is_signed_borrower;
                $loan->link_sign_document = $aggreementDocumentations->link_sign_document;
            }
        
            if($loan->loan_status == 'Cicilan Sedang Berjalan'){

                $onProgress = $loanservice->onProgressLoan($loan->id);
                $statusCode = 200;
                $response = [
                        'error' => false,
                        'message' => 'Status Pinjaman',
                        'dataLoan' => [$onProgress],
                ];  
            } 
             else{
                $statusCode = 200;
                $response = [
                        'error' => false,
                        'message' => 'Status Pinjaman',
                        'dataLoan' => [$loan],
                ];
            }
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Status Pinjaman',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
        
    }

    public function viewInformation(Request $request, $name)
    {   
        try{
            $inform = CompanyInformations::where('name','=',$name)->get();
            $statusCode = 200;
                $response = [
                'error' => false,
                'data' => $inform,
            ];
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Informasi Perusahaan',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function viewNotification(Request $request, $id)
    {   
        try{
            $notif = BorrowerNotifications::where('id_borrower', $id)
            ->orderBy('created_at', 'desc')
            ->get();
            if(!$notif){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            }else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'dataNotifications' => $notif,
            ];
            }
            
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function deleteNotification(Request $request, $id_notif)
    {   
        try{
            $notif = BorrowerNotifications::where('id', $id_notif)
                    ->first();
            $notif->delete();
            $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Notifikasi Dihapus',
            ];
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function detailNotificationStatus(Request $request, $id_notif)
    {   
        try{
            // $notif = BorrowerNotifications::where('id', $id_notif)->first();
            $service = new BorrowerService();
            $notif = $service->detailNotificationStatus($id_notif);
            if(!$notif){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            } else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Detail Notifikasi',
                'dataDetailNotificationStatus' => [$notif],
            ];
            }
            
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function detailNotificationWithdraw(Request $request, $id_notif)
    {   
        try{
            // $notif = BorrowerNotifications::where('id', $id_notif)->first();
            $service = new BorrowerService();
            $notif = $service->detailNotificationWithdraw($id_notif);
            if(!$notif){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            } else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Detail Notifikasi',
                'dataDetailNotificationWithdraw' => [$notif],
            ];
            }
            
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function detailNotificationPayment(Request $request, $id_notif)
    {   
        try{
            // $notif = BorrowerNotifications::where('id', $id_notif)->first();
            $service = new BorrowerService();
            $notif = $service->detailNotificationPayment($id_notif);
            if(!$notif){
                $statusCode = 404;
                $response = [
                'error' => true,
                'message' => 'Data Tidak Ada',
            ];
            } else{
                $statusCode = 200;
                $response = [
                'error' => false,
                'message' => 'Detail Notifikasi',
                'dataDetailNotificationPayment' => [$notif],
            ];
            }
            
        } catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal Menampilkan Notifikasi',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    // view all notifications that have been read

    // public function isReadNotification(Request $request, $id)
    // {
    //     try{    
    //         $notification= BorrowerNotifications::where('id_borrower', $id)
    //         ->get();
    //         if(!$notification){
    //             $statusCode = 404;
    //             $response = [
    //                 'error' => true,
    //                 'message' => 'Data Tidak Ada',
    //             ];
    //         }else{
    //             $statusCode = 200;
    //             $response = [
    //                 'error' => false,
    //                 'message' => 'Notifikasi Yang Belum Dibaca',
    //                 'dataNotification' =>$notification,
    //             ];
    //         }
    //     }catch (Exception $ex){
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Gagal Menampilkan Notifikasi',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    // }

    public function viewLoanHistory(Request $request, $id)
    {
        try{
            $listStatus = Loans::with('detailBorrower')->where('id_borrower',$id)
            ->where('loan_status', '=', $request->loan_status)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Tampilkan Riwayat Pinjaman',
                'data' => $listStatus,
            ];
        }catch (Exception $ex){
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Tampilan Jenis Pinjaman Gagal',
            ];
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getLastEducation()
    {
        try{
            $listEducation = MasterCode::where('category','=', 'Education')
                                            ->where('is_active', 1)->get();
            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'list pendidikan terakhir',
                'dataLastEducation' => $listEducation,
            ];  
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getTkbResult(){
        try{
            $borrowerService= new BorrowerService();
            $loan = $borrowerService->countinstallmentLate();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Berhasil',
                'dataTKB' => [$loan]
            ]; 
        } catch (Exception $ex) {
            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Gagal',
            ];  
        }
        finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    // public function viewAllLoans($id){
    //     try{
    //         $loan= Loans::where('id_borrower',$id)->get();
    //         $group= DB::table('group_loans')
    //                 ->join('loans')
    //                 ->joinwhere('id_borrower',$id)->get();

    //         $statusCode = 200;
    //         $response = [
    //             'error' => false,
    //             'message' => 'Semua Pinjaman',
    //             // 'dataAllLoan' => $loan,
    //         ];

    //     }catch (Exception $ex){
    //         $statusCode = 404;
    //         $response = [
    //             'error' => true,
    //             'message' => 'Tampilan Jenis Pinjaman Gagal',
    //         ];
    //     }
    //     finally {
    //         return response($response,$statusCode)->header('Content-Type','application/json');
    //     }
    // }

}