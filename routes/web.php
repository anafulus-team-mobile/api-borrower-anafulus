<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//// api for mobile-borrower 
//dashboard
$router->get('/view-promotion', 'HomeController@viewPromotion');
$router->get('/view-loanProgress/{id}', 'HomeController@viewLoanProgress');
$router->get('/view-profile/{id}', 'HomeController@viewProfile');
$router->get('/view-loan/{id}', 'HomeController@viewLoan');
$router->get('/view-information/{name}','HomeController@viewInformation');
$router->get('/view-notification/{id}','HomeController@viewNotification');
$router->get('/detail-notification-status/{id_notif}','HomeController@detailNotificationStatus');
$router->get('/detail-notification-withdraw/{id_notif}','HomeController@detailNotificationWithdraw');
$router->get('/detail-notification-payment/{id_notif}','HomeController@detailNotificationPayment');
$router->get('/view-loanHistory/{id}', 'HomeController@viewLoanHistory');
$router->get('/view-lastLoanStatus/{id}', 'HomeController@viewLoanStatus');
// $router->get('/isRead-notification/{id}','HomeController@isReadNotification');
$router->delete('/delete-notification/{id_notif}', 'HomeController@deleteNotification');
$router->get('/get-last-education', 'HomeController@getLastEducation');
$router->get('/tkb-result', 'HomeController@getTkbResult');

// $router->get('/view-allLoans/{id}','HomeController@viewAllLoans');

//auth - Borrower
$router->post('/register', 'AuthController@register');
$router->post('/register-google', 'AuthController@registerGoogle');
$router->post('/register-facebook', 'AuthController@registerFacebook');
$router->post('/login', 'AuthController@login');
$router->get('/login-google', 'AuthController@loginGoogle');
$router->get('/login-facebook', 'AuthController@loginFacebook');

$router->put('/change-pin/{id}', 'AuthController@changePin');
$router->get('/check-pin/{id}', 'AuthController@checkPin');
$router->put('/change-phoneNumber', 'AuthController@changePhoneNumber');
$router->put('/update-phoneNumber/{id}', 'AuthController@updatePhoneNumber');
$router->put('/forget-pin/{id}', 'AuthController@forgetPin');

// payment partner
$router->get('/get-payment-partner', 'BorrowerController@getPaymentPartner');

// Borrower
$router->put('/update-personalData/{id}', 'BorrowerController@updatePersonalData');
$router->put('/update-address/{id}', 'BorrowerController@updateAddress');
$router->post('/update-requirement/{id}', 'BorrowerController@updateRequirement');
$router->get('/view-balance/{id}', 'BorrowerController@viewBalance');
$router->put('/update-bankAccount/{id}', 'BorrowerController@updateBankAccount');
$router->post('/create-bankAccount/{id}', 'BorrowerController@createBankAccount');
$router->put('/edit-bankAccount/{id}', 'BorrowerController@editBankAccount');
$router->delete('/delete-bankAccount/{id}', 'BorrowerController@deleteBankAccount');
$router->get('/view-bankAccount/{id}', 'BorrowerController@viewBankAccount');
$router->get('/view-detail-bankAccount/{id}', 'BorrowerController@viewDetailBankAccount');
// Tarik Dana dari saldo ke bank
$router->put('/update-finance-balance/{id}', 'BorrowerController@updateFinanceBalance');
// Tarik Dana dari pinjaman ke saldo
$router->put('/withdraw-loan/{id_borrower}', 'BorrowerController@withdrawLoan');

$router->post('/update-photo-profile/{id}', 'BorrowerController@updatePhotoProfile');

$router->get('/list-provinces', 'BorrowerController@viewProvince');
$router->get('/list-regencies/{idProvince}', 'BorrowerController@viewRegency');
$router->get('/list-subdistricts/{idRegence}', 'BorrowerController@viewSubdistrics');
$router->get('/list-village/{idSubdistrict}', 'BorrowerController@viewVillage');

$router->get('/view-installmentNominal/{id}', 'BorrowerController@viewInstallmentNominal');
$router->get('/view-VirtualAccount', 'BorrowerController@viewVirtualAccount');
$router->get('/view-detailVirtualAccount/{id}', 'BorrowerController@detailVirtualAccount');
$router->get('/view-help/{id}', 'BorrowerController@viewHelp');
$router->get('/view-rating/{id}', 'BorrowerController@viewRating');
$router->put('/update-readNotification/{id}', 'BorrowerController@updateReadNotification');
// $router->put('/update-readNotification-headAgent/{id}', 'BorrowerController@updateReadNotificationHead');
$router->get('/notification-view-loan-status/{idBorrower}', 'LoanController@viewLoanStatus');
$router->get('/view-regulerLoan/{id_loan}', 'BorrowerController@regulerLoan');
$router->get('/view-installmentLoan/{id_loan}', 'BorrowerController@installmentLoan');
// $router->put('/withdraw-loan/{id_borrower}', 'BorrowerController@withdrawLoan');
$router->get('/all-borrowers', 'BorrowerController@getAllBorrowers');
$router->get('/get-variable-formula/{category}', 'BorrowerController@getVariableFormula');

// Loan
$router->post('/payment-loan-history/create/{idBorrower}', 'LoanController@createPaymentLoanHistory');
$router->get('/payment-loan-history/view/{idBorrower}', 'LoanController@getPaymentHistory');

//Midtrans
$router->post('/payment-loan-history/update', 'LoanController@updatePaymentLoanHistory');
$router->get('/testSendDocument', 'LoanController@testSendDocument');


$router->post('/loan/create', 'LoanController@createLoan');
$router->put('/reject-loan/{idBorrower}', 'LoanController@rejectLoan');
// $router->put('/cancel-reject-loan/{idBorrower}', 'LoanController@cancelRejectLoan');
$router->get('/view-by-subdistrict/{idSubdistrict}', 'LoanController@viewBySubdistrict');
$router->get('/view-by-agent-code/{agentCode}', 'LoanController@viewByAgentCode');
$router->get('/installmentCategory/{category}', 'LoanController@installmentCategory');
$router->get('/filterByStatus/{id}', 'LoanController@filterByStatus');
$router->get('/filterAllByStatus/{id}', 'LoanController@filterAllByStatus');
$router->get('/view-loan/{id}', 'LoanController@viewLoanPrincipal');
$router->put('/update-balance/{id}', 'LoanController@updateBalance');
$router->get('/filterByCategory/{category}', 'LoanController@filterByCategory');
$router->get('/loanByStatus/{id}', 'LoanController@loanByStatus');
$router->get('/installmentStatus/{id}', 'LoanController@installmentStatus');
$router->put('/cancel-loan-before-loan-approve/{idLoan}', 'LoanController@cancelLoanBeforeLoanApprove');
$router->get('/detail-installment-status/{id}', 'LoanController@detailInstallmentStatus');
$router->put('/approve-loan-negotiation/{idLoan}', 'LoanController@approveLoanNegotiation');

$router->put('/pay-with-finance-balance', 'LoanController@payWithSaldo');
$router->get('/get-masterCode', 'LoanController@getMasterCode');
